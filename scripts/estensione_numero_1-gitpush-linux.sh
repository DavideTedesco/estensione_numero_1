#!/usr/bin/env bash

# bn-tesi-tedesco-gitpush-linux.sh
#
# use it by terminal typing:
# bash bn-tesi-tedesco-gitpush-linux.sh

cd /home/davide/gitlab/DavideTedesco/estensione_numero_1
git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push
