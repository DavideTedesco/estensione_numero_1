#!/bin/sh

#  estensione_numero_1-gitpush.sh
#
# use it by terminal typing:
# bash estensione_numero_1-gitpush.sh

cd /Users/davide/gitlab/DavideTedesco/estensione_numero_1

git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push
