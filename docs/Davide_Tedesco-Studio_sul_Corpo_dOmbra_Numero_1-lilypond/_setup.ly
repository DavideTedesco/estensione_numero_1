% !TEX encoding = UTF-8 Unicode
% !TEX TS-program = LilyPond

% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% -REVA190913------------------- PACCHETTO DI PERSONALIZZAZIONI DI LILYPOND - 
% ------------------------------ DEDICATO ALLA SCRITTURA DELLA MUSICA -------
% ------------------------------ CONTEMPORANEA ED ELETTROACUSTICA -----------
% ---------------------------------------------------------------------------
% ------------------------------ GIUSEPPE SILVI -----------------------------
% ------------------------------ MAZZAFRUSTU AT GMAIL DOT COM ---------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------
% ------------------------------ IMPOSTAZIONI GENERALI ----------------------
% ---------------------------------------------------------------------------
% ---------------------------------------------------------------------------

%\version "2.18.2"

% ---------------------------------------------------------------------------
% -------------------------------------------------- IMPOSTAZIONI GENERICHE -
% ---------------------------------------------------------------------------

 
 
#(set-global-staff-size 18)


\paper {
  % annotate-spacing = ##f % stampa le distanze tra gli oggetti

  #(set-paper-size "a4")
  
  #(define fonts
    (set-global-fonts
      #:music "ross"
      #:brace "ross"
      #:roman "IMFellEnglish"
      #:sans "sans-serif"
      #:typewriter "monospace"
      #:factor (/ staff-height pt 20)
  ))
  
  #(include-special-characters)
  
  indent = 2\cm
  two-sided=##t
  binding-offset = 4\mm
  inner-margin =  1.2\cm
  outer-margin = 1.6\cm
  top-margin = 1.2\cm
  bottom-margin = 1\cm

  %first-page-number = 5
  %print-page-number = ##f
  %print-first-page-number = ##f

  %oddHeaderMarkup = ""
  %evenHeaderMarkup = ""
  
  %from papera4P_with_page_number.ly
  
  %page number customization
                print-page-number = ##t
                print-first-page-number = ##t
                %centering page numbers
                oddHeaderMarkup = \markup \null
    		evenHeaderMarkup = \markup \null
    		evenHeaderMarkup = \markup \null
    		oddFooterMarkup = \markup {
     		 \fill-line {
       		 \on-the-fly \print-page-number-check-first
       		 \fromproperty #'page:page-number-string
     		 }
    	  	 }

   min-systems-per-page = 8
   max-systems-per-page = 8

  ragged-last-bottom = ##f
  ragged-bottom = ##f

  %slashseparator=========================

   mySlashSeparator = \markup {
    \center-align
    \vcenter
    \combine
    \beam #5.0 #0.5 #0.48
    \raise #1
    \beam #5.0 #0.5 #0.48
  }

  %to use for various instruments
  %system-separator-markup = \mySlashSeparator

  
}
myBreak = {\break \bar "|"}
