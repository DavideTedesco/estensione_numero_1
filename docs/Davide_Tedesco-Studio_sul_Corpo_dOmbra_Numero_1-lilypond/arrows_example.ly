%% http://lsr.di.unimi.it/LSR/Item?id=1026
%% see also http://lsr.di.unimi.it/LSR/Item?id=650
%% Credits PPS on Feb. 2016

%%%% Snippet start here:

%%% "arrows.ily %%%

#(define-markup-command 
   (arrow layout props 
     type ;; "open" "close" "huron" "long" "large" "wings" "relief"
     fletching? ;; #t or #f
     axis ;; X or Y
     direction ;; RIGHT or LEFT, UP or DOWN, 
     a-length ;; any number >= #0
     boldness) ;; #0 < any number < #1
   (string? boolean? ly:dir? ly:dir? number? number?)
  "Draw arrows with variable length & boldness."
  (define fletching #t)  
  (interpret-markup layout props
    (cond 
     
;; "open" optimal thickness #0.14
      ((and (string=? type "open")(and (eq? axis X)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto  -0.4  0.0  -0.7  0.18  -0.94  0.44)
                 (moveto    0.0  0.0)
                 (curveto  -0.4  0.0  -0.7 -0.18  -0.94 -0.44)
                 (moveto    0.0  0.0)
                 (lineto   ,(+ (* a-length -1) 1) 0)
                 (rmoveto   1.44  0.0)
                 (rcurveto -0.4   0.0 -0.7  0.18 -0.94  0.44)
                 (rmoveto   0.94 -0.44)
                 (rcurveto -0.4   0.0 -0.7 -0.18 -0.94 -0.44)
                 (rmoveto   0.44  0.44)
                 (rcurveto -0.4   0.0 -0.7  0.18 -0.94  0.44)
                 (rmoveto   0.94 -0.44)
                 (rcurveto -0.4   0.0 -0.7 -0.18 -0.94 -0.44)
                 (rmoveto   0.44  0.44)
                 (rcurveto -0.4   0.0 -0.7  0.18 -0.94  0.44)
                 (rmoveto   0.94 -0.44)
                 (rcurveto -0.4   0.0 -0.7 -0.18 -0.94 -0.44)
                 (rmoveto   0.44  0.44)
                 (rcurveto -0.4   0.0 -0.7  0.18 -0.94  0.44)
                 (rmoveto   0.94 -0.44)
                 (rcurveto -0.4   0.0 -0.7 -0.18 -0.94 -0.44)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto  -0.4  0.0  -0.7  0.18  -0.94  0.44)
                 (moveto    0.0  0.0)
                 (curveto  -0.4  0.0  -0.7 -0.18  -0.94 -0.44)
                 (moveto    0.0  0.0)
                 (lineto   ,(* a-length -1) 0)))))))
     
      ((and (string=? type "open")(and (eq? axis X)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto   0.4  0.0   0.7  0.18   0.94  0.44)
                 (moveto    0.0  0.0)
                 (curveto   0.4  0.0   0.7 -0.18   0.94 -0.44)
                 (moveto    0.0  0.0)
                 (lineto   ,(- a-length 1) 0)
                 (rmoveto  -1.44   0.0)
                 (rcurveto  0.4   0.0  0.7  0.18  0.94  0.44)
                 (rmoveto  -0.94 -0.44)
                 (rcurveto  0.4   0.0  0.7 -0.18  0.94 -0.44)
                 (rmoveto  -0.44  0.44)
                 (rcurveto  0.4   0.0  0.7  0.18  0.94  0.44)
                 (rmoveto  -0.94 -0.44)
                 (rcurveto  0.4   0.0  0.7 -0.18  0.94 -0.44)
                 (rmoveto  -0.44  0.44)
                 (rcurveto  0.4   0.0  0.7  0.18  0.94  0.44)
                 (rmoveto  -0.94 -0.44)
                 (rcurveto  0.4   0.0  0.7 -0.18  0.94 -0.44)
                 (rmoveto  -0.44  0.44)
                 (rcurveto  0.4   0.0  0.7  0.18  0.94  0.44)
                 (rmoveto  -0.94 -0.44)
                 (rcurveto  0.4   0.0  0.7 -0.18  0.94 -0.44)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto   0.4  0.0   0.7  0.18   0.94  0.44)
                 (moveto    0.0  0.0)
                 (curveto   0.4  0.0   0.7 -0.18   0.94 -0.44)
                 (moveto    0.0  0.0)
                 (lineto   ,a-length 0)))))))
      
      ((and (string=? type "open")(and (eq? axis Y)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto   0.0 -0.4  0.18  -0.7   0.44 -0.94)
                 (moveto    0.0  0.0)
                 (curveto   0.0 -0.4 -0.18  -0.7  -0.44 -0.94)
                 (moveto    0.0  0.0)
                 (lineto    0.0  ,(+ (* a-length -1) 1))
                 (rmoveto   0.0   1.44)
                 (rcurveto  0.0  -0.4  0.18 -0.7  0.44 -0.94)
                 (rmoveto  -0.44  0.94)
                 (rcurveto  0.0  -0.4 -0.18 -0.7 -0.44 -0.94)
                 (rmoveto   0.44  0.44)
                 (rcurveto  0.0  -0.4  0.18 -0.7  0.44 -0.94)
                 (rmoveto  -0.44  0.94)
                 (rcurveto  0.0  -0.4 -0.18 -0.7 -0.44 -0.94)
                 (rmoveto   0.44  0.44)
                 (rcurveto  0.0  -0.4  0.18 -0.7  0.44 -0.94)
                 (rmoveto  -0.44  0.94)
                 (rcurveto  0.0  -0.4 -0.18 -0.7 -0.44 -0.94)
                 (rmoveto   0.44  0.44)
                 (rcurveto  0.0  -0.4  0.18 -0.7  0.44 -0.94)
                 (rmoveto  -0.44  0.94)
                 (rcurveto  0.0  -0.4 -0.18 -0.7 -0.44 -0.94)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto   0.0 -0.4  0.18  -0.7   0.44 -0.94)
                 (moveto    0.0  0.0)
                 (curveto   0.0 -0.4 -0.18  -0.7  -0.44 -0.94)
                 (moveto    0.0  0.0)
                 (lineto    0.0 ,(* a-length -1))))))))
     
      ((and (string=? type "open")(and (eq? axis Y)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto   0.0  0.4  0.18   0.7   0.44  0.94)
                 (moveto    0.0  0.0)
                 (curveto   0.0  0.4 -0.18   0.7  -0.44  0.94)
                 (moveto    0.0  0.0)
                 (lineto    0.0  ,(- a-length 1))
                 (rmoveto   0.0  -1.44)
                 (rcurveto  0.0   0.4  0.18  0.7  0.44  0.94)
                 (rmoveto  -0.44 -0.94)
                 (rcurveto  0.0   0.4 -0.18  0.7 -0.44  0.94)
                 (rmoveto   0.44 -0.44)
                 (rcurveto  0.0   0.4  0.18  0.7  0.44  0.94)
                 (rmoveto  -0.44 -0.94)
                 (rcurveto  0.0   0.4 -0.18  0.7 -0.44  0.94)
                 (rmoveto   0.44 -0.44)
                 (rcurveto  0.0   0.4  0.18  0.7  0.44  0.94)
                 (rmoveto  -0.44 -0.94)
                 (rcurveto  0.0   0.4 -0.18  0.7 -0.44  0.94)
                 (rmoveto   0.44 -0.44)
                 (rcurveto  0.0   0.4  0.18  0.7  0.44  0.94)
                 (rmoveto  -0.44 -0.94)
                 (rcurveto  0.0   0.4 -0.18  0.7 -0.44  0.94)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto   0.0  0.4  0.18   0.7   0.44  0.94)
                 (moveto    0.0  0.0)
                 (curveto   0.0  0.4 -0.18   0.7  -0.44  0.94)
                 (moveto    0.0  0.0)
                 (lineto    0.0 ,a-length)))))))
     
;; "close" optimal thickness #0.14
      ((and (string=? type "closed")(and (eq? axis X)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.0   0.0)
                 (curveto  -0.06  0.00  -0.55 -0.07  -0.97 -0.47)
                 (curveto  -0.99 -0.38  -0.92 -0.20  -0.92 -0.02)
                 (lineto   ,(+ (* a-length -1) 3.03) -0.02)
                 (rcurveto -0.06  0.00  -0.55 -0.07  -0.97 -0.48)
                 (rlineto  -2.03  0.00)
                 (rcurveto  0.14  0.14   0.48  0.41   0.95  0.50)
                 (rcurveto -0.06  0.00  -0.55  0.07  -0.95  0.50)
                 (rlineto   2.03  0.00)
                 (rcurveto  0.14 -0.14   0.48 -0.41   0.95 -0.48)
                 (lineto   ,(+ (* a-length -1) 3.03)  0.02)
                 (lineto   -0.92  0.02)
                 (curveto  -0.92  0.20  -0.99  0.38  -0.97  0.47)
                 (curveto  -0.55  0.07  -0.06  0.00   0.00  0.00)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto -0.06  0.00  -0.55 -0.07  -0.97 -0.47)
                 (curveto -0.99 -0.38  -0.92 -0.20  -0.92 -0.02)
                 (lineto ,(* a-length -1) -0.02)
                 (lineto ,(* a-length -1)  0.02)
                 (lineto -0.92  0.02)
                 (curveto -0.92  0.20  -0.99  0.38  -0.97  0.47)
                 (curveto -0.55  0.07  -0.06  0.00   0.00  0.00)))))))
     
      ((and (string=? type "closed")(and (eq? axis X)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.0   0.0)
                 (curveto   0.06  0.00  0.55  -0.07  0.97 -0.47)
                 (curveto   0.99 -0.38  0.92  -0.20  0.92 -0.02)
                 (lineto   ,(- a-length 3.03) -0.02)
                 (rcurveto  0.06  0.00  0.55  -0.07  0.97 -0.48)
                 (rlineto   2.03  0.00)
                 (rcurveto -0.14  0.14 -0.48   0.41 -0.95  0.50)
                 (rcurveto  0.06  0.00  0.55   0.07  0.95  0.50)
                 (rlineto  -2.03  0.00)
                 (rcurveto -0.14 -0.14 -0.48  -0.41 -0.95 -0.48)
                 (lineto   ,(- a-length 3.03)  0.02)
                 (lineto    0.92  0.02)
                 (curveto   0.92  0.20  0.99   0.38  0.97  0.47)
                 (curveto   0.55  0.07  0.06   0.00  0.00  0.00)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
                `((moveto   0.0   0.0)
                 (curveto  0.06  0.00  0.55 -0.07  0.97 -0.47)
                 (curveto  0.99 -0.38  0.92 -0.20  0.92 -0.02)
                 (lineto   ,a-length -0.02)
                 (lineto   ,a-length  0.02)
                 (lineto   0.92  0.02)
                 (curveto  0.92  0.20  0.99  0.38  0.97  0.47)
                 (curveto  0.55  0.07  0.06  0.00  0.00  0.00)))))))
      
      ((and (string=? type "closed")(and (eq? axis Y)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.00  0.00)
                 (curveto   0.00 -0.06  -0.07 -0.55  -0.47 -0.97)
                 (curveto  -0.38 -0.99  -0.20 -0.92  -0.02 -0.92)
                 (lineto   -0.02  ,(+ (* a-length -1) 3.03))
                 (rcurveto  0.00 -0.06  -0.07 -0.55  -0.48 -0.97)
                 (rlineto   0.00 -2.03)
                 (rcurveto  0.14  0.14   0.41  0.48   0.50  0.95 )
                 (rcurveto  0.00 -0.06   0.07 -0.55   0.50 -0.95)
                 (rlineto   0.00  2.03)
                 (rcurveto -0.14  0.14  -0.41  0.48  -0.48  0.95)
                 (lineto    0.02  ,(+ (* a-length -1) 3.03))
                 (lineto    0.02 -0.92)
                 (curveto   0.20 -0.92   0.38 -0.99   0.47 -0.97)
                 (curveto   0.07 -0.55   0.00 -0.06   0.00  0.00)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (curveto  0.00 -0.06  -0.07 -0.55 -0.47 -0.97)
                 (curveto -0.38 -0.99  -0.20 -0.92 -0.02 -0.92)
                 (lineto  -0.02  ,(* a-length -1))
                 (lineto   0.02  ,(* a-length -1))
                 (lineto   0.02 -0.92 )
                 (curveto  0.20 -0.92   0.38 -0.99   0.47 -0.97)
                 (curveto  0.07 -0.55   0.00 -0.06   0.00  0.00)))))))
     
      ((and (string=? type "closed")(and (eq? axis Y)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto  0.00  0.06  -0.07  0.55 -0.47  0.97)
                 (curveto -0.38  0.99  -0.20  0.92 -0.02  0.92)
                 (lineto  -0.02  ,(- a-length 3.03))
                 
                 (rcurveto  0.00  0.06  -0.07  0.55  -0.48  0.97)
                 (rlineto   0.00  2.03)
                 (rcurveto  0.14 -0.14   0.41 -0.48   0.50 -0.95 )
                 (rcurveto  0.00  0.06   0.07  0.55   0.50  0.95)
                 (rlineto   0.00 -2.03)
                 (rcurveto -0.14 -0.14  -0.41 -0.48  -0.48 -0.95)
                 
                 (lineto   0.02  ,(- a-length 3.03))
                 (lineto   0.02  0.92 )
                 (curveto  0.20  0.92   0.38  0.99   0.47  0.97)
                 (curveto  0.07  0.55   0.00  0.06   0.00  0.00)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (curveto  0.00  0.06  -0.07  0.55 -0.47  0.97)
                 (curveto -0.38  0.99  -0.20  0.92 -0.02  0.92)
                 (lineto  -0.02  ,a-length)
                 (lineto   0.02  ,a-length)
                 (lineto   0.02  0.92 )
                 (curveto  0.20  0.92   0.38  0.99   0.47  0.97)
                 (curveto  0.07  0.55   0.00  0.06   0.00  0.00)))))))    
      
;; "huron" optimal thickness #0.15
      ((and (string=? type "huron")(and (eq? axis X)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto   -0.6 -0.5)
                 (moveto    0.0  0.0)
                 (lineto   -0.6  0.5)
                 (moveto    0.0  0.0)
                 (lineto   ,(+ (* a-length -1) 0.6) 0)
                 (rmoveto   1.6  0.0)
                 (rlineto  -0.6  -0.5)
                 (rmoveto   0.6   0.5)
                 (rlineto  -0.6   0.5)
                 (rmoveto   0.2  -0.5)
                 (rlineto  -0.6  -0.5)
                 (rmoveto   0.6   0.5)
                 (rlineto  -0.6   0.5)
                 (rmoveto   0.2  -0.5)
                 (rlineto  -0.6  -0.5)
                 (rmoveto   0.6   0.5)
                 (rlineto  -0.6   0.5)
                 (rmoveto   0.2  -0.5)
                 (rlineto  -0.6  -0.5)
                 (rmoveto   0.6   0.5)
                 (rlineto  -0.6   0.5)
                 (rmoveto   0.2  -0.5)
                 (rlineto  -0.6  -0.5)
                 (rmoveto   0.6   0.5)
                 (rlineto  -0.6   0.5)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto   -0.6 -0.5)
                 (moveto    0.0  0.0)
                 (lineto   -0.6  0.5)
                 (moveto    0.0  0.0)
                 (lineto   ,(* a-length -1) 0)))))))
     
      ((and (string=? type "huron")(and (eq? axis X)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto    0.6 -0.5)
                 (moveto    0.0  0.0)
                 (lineto    0.6  0.5)
                 (moveto    0.0  0.0)
                 (lineto   ,(- a-length 0.6) 0)
                 (rmoveto  -1.6   0.0)
                 (rlineto   0.6  -0.5)
                 (rmoveto  -0.6   0.5)
                 (rlineto   0.6   0.5)
                 (rmoveto  -0.2  -0.5)
                 (rlineto   0.6  -0.5)
                 (rmoveto  -0.6   0.5)
                 (rlineto   0.6   0.5)
                 (rmoveto  -0.2  -0.5)
                 (rlineto   0.6  -0.5)
                 (rmoveto  -0.6   0.5)
                 (rlineto   0.6   0.5)
                 (rmoveto  -0.2  -0.5)
                 (rlineto   0.6  -0.5)
                 (rmoveto  -0.6   0.5)
                 (rlineto   0.6   0.5)
                 (rmoveto  -0.2  -0.5)
                 (rlineto   0.6  -0.5)
                 (rmoveto  -0.6   0.5)
                 (rlineto   0.6   0.5)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto    0.6 -0.5)
                 (moveto    0.0  0.0)
                 (lineto    0.6  0.5)
                 (moveto    0.0  0.0)
                 (lineto   ,a-length 0)))))))
      
      ((and (string=? type "huron")(and (eq? axis Y)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto   -0.5 -0.6)
                 (moveto    0.0  0.0)
                 (lineto    0.5 -0.6)
                 (moveto    0.0  0.0)
                 (lineto    0.0 ,(+ (* a-length -1) 0.6))
                 (rmoveto   0.0   1.6)
                 (rlineto   -0.5 -0.6)
                 (rmoveto   0.5   0.6)
                 (rlineto   0.5  -0.6)
                 (rmoveto  -0.5   0.2)
                 (rlineto  -0.5  -0.6)
                 (rmoveto   0.5   0.6)
                 (rlineto   0.5  -0.6)
                 (rmoveto  -0.5   0.2)
                 (rlineto  -0.5  -0.6)
                 (rmoveto   0.5   0.6)
                 (rlineto   0.5  -0.6)
                 (rmoveto  -0.5   0.2)
                 (rlineto  -0.5  -0.6)
                 (rmoveto   0.5   0.6)
                 (rlineto   0.5  -0.6)
                 (rmoveto  -0.5   0.2)
                 (rlineto  -0.5  -0.6)
                 (rmoveto   0.5   0.6)
                 (rlineto   0.5  -0.6)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto   -0.5 -0.6)
                 (moveto    0.0  0.0)
                 (lineto    0.5 -0.6)
                 (moveto    0.0  0.0)
                 (lineto    0.0 ,(* a-length -1))))))))
     
      ((and (string=? type "huron")(and (eq? axis Y)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto   -0.5  0.6)
                 (moveto    0.0  0.0)
                 (lineto    0.5  0.6)
                 (moveto    0.0  0.0)
                 (lineto    0.0 ,(- a-length 0.6))
                 (rmoveto   0.0  -1.6)
                 (rlineto   -0.5  0.6)
                 (rmoveto   0.5  -0.6)
                 (rlineto   0.5   0.6)
                 (rmoveto  -0.5  -0.2)
                 (rlineto  -0.5   0.6)
                 (rmoveto   0.5  -0.6)
                 (rlineto   0.5   0.6)
                 (rmoveto  -0.5  -0.2)
                 (rlineto  -0.5   0.6)
                 (rmoveto   0.5  -0.6)
                 (rlineto   0.5   0.6)
                 (rmoveto  -0.5  -0.2)
                 (rlineto  -0.5   0.6)
                 (rmoveto   0.5  -0.6)
                 (rlineto   0.5   0.6)
                 (rmoveto  -0.5  -0.2)
                 (rlineto  -0.5   0.6)
                 (rmoveto   0.5  -0.6)
                 (rlineto   0.5   0.6)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto   -0.5  0.6)
                 (moveto    0.0  0.0)
                 (lineto    0.5  0.6)
                 (moveto    0.0  0.0)
                 (lineto    0.0 ,a-length)))))))  
 
;; "long" optimal thickness #0.07
      ((and (string=? type "long")(and (eq? axis X)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto   -2.50 -0.60)
                 (curveto  -2.40 -0.40 -2.35 -0.15 -2.35 -0.05)
                 (lineto  ,(* (- a-length 2.40) -1) -0.05)
                 (rlineto  -0.30 -0.55)
                 (rlineto  -2.10  0.00)
                 (rlineto   0.30  0.60)
                 (rlineto  -0.30  0.60)
                 (rlineto   2.10  0.00)
                 (rlineto   0.30 -0.55)
                 (lineto  ,(* (- a-length 2.40) -1)  0.05)
                 (lineto   -2.35  0.05)
                 (curveto  -2.35  0.10 -2.40  0.40 -2.50  0.60)
                 (lineto    0.00  0.00)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.00  0.00)
                 (lineto   -2.50 -0.60)
                 (curveto  -2.40 -0.40 -2.35 -0.15 -2.35 -0.05)
                 (lineto   ,(* a-length -1) -0.05)
                 (lineto   ,(* a-length -1)  0.05)
                 (lineto   -2.35  0.05)
                 (curveto  -2.35  0.10 -2.40  0.40 -2.50  0.60)
                 (lineto    0.00  0.00)
                 (closepath)))))))
     
      ((and (string=? type "long")(and (eq? axis X)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.00  0.00)
                 (lineto    2.50 -0.60)
                 (curveto   2.40 -0.40  2.35 -0.15  2.35 -0.05)
                 (lineto  ,(- a-length 2.40) -0.05)
                 (rlineto   0.30 -0.55)
                 (rlineto   2.10  0.00)
                 (rlineto  -0.30  0.60)
                 (rlineto   0.30  0.60)
                 (rlineto  -2.10  0.00)
                 (rlineto  -0.30 -0.55)
                 (lineto  ,(- a-length 2.40)  0.05)
                 (lineto    2.35  0.05)
                 (curveto   2.35  0.10  2.40  0.40  2.50  0.60)
                 (lineto    0.00  0.00)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.00  0.00)
                 (lineto    2.50 -0.60)
                 (curveto   2.40 -0.40  2.35 -0.15  2.35 -0.05)
                 (lineto   ,a-length -0.05)
                 (lineto   ,a-length  0.05)
                 (lineto    2.35  0.05)
                 (curveto   2.35  0.10  2.40  0.40  2.50  0.60)
                 (lineto    0.00  0.00)
                 (closepath)))))))
      
      ((and (string=? type "long")(and (eq? axis Y)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.00 0.00)
                 (lineto   -0.60 -2.50)
                 (curveto  -0.40 -2.40 -0.15 -2.35 -0.05 -2.35)
                 (lineto   -0.05 ,(+ (* a-length -1) 2.40))
                 (rlineto  -0.55 -0.30)
                 (rlineto   0.00 -2.10)
                 (rlineto   0.60  0.30)
                 (rlineto   0.60 -0.30)
                 (rlineto   0.00  2.10)
                 (rlineto  -0.55  0.30)
                 (lineto    0.05 ,(+ (* a-length -1) 2.40))
                 (lineto    0.05 -2.35)
                 (curveto   0.10 -2.35  0.40 -2.40  0.60 -2.50)
                 (lineto    0.00  0.00)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.00  0.00)
                 (lineto   -0.60 -2.50)
                 (curveto  -0.40 -2.40 -0.15 -2.35 -0.05 -2.35)
                 (lineto   -0.05 ,(* a-length -1))
                 (lineto    0.05 ,(* a-length -1))
                 (lineto    0.05 -2.35)
                 (curveto   0.10 -2.35  0.40  -2.40  0.60 -2.50)
                 (lineto    0.00  0.00)
                 (closepath)))))))
     
      ((and (string=? type "long")(and (eq? axis Y)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.00  0.00)
                 (lineto   -0.60  2.50)
                 (curveto  -0.40  2.40  -0.15  2.35 -0.05 2.35)
                 (lineto   -0.05 ,(- a-length 2.40))
                 (rlineto  -0.55  0.30)
                 (rlineto   0.00  2.10)
                 (rlineto   0.60 -0.30)
                 (rlineto   0.60  0.30)
                 (rlineto   0.00 -2.10)
                 (rlineto  -0.55 -0.30)
                 (lineto    0.05 ,(- a-length 2.40))
                 (lineto    0.05  2.35)
                 (curveto   0.10  2.35  0.40  2.40  0.60 2.50)
                 (lineto    0.00  0.00)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.00  0.00)
                 (lineto    -0.60 2.50)
                 (curveto  -0.40   2.40 -0.15  2.35 -0.05 2.35)
                 (lineto   -0.05 ,a-length)
                 (lineto    0.05 ,a-length)
                 (lineto    0.05  2.35)
                 (curveto    0.10  2.35  0.40  2.40  0.60 2.50)
                 (lineto    0.00  0.00)
                 (closepath)))))))
      
;; "large" optimal thickness #0.07
      ((and (string=? type "large")(and (eq? axis X)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto    0.0  0.0)
                 (lineto  -1.5  2.0)
                 (lineto  -1.5  1.0)
                 (lineto  ,(* a-length -1)  1.5)
                 (rlineto  1.0 -1.5)
                 (rlineto -1.0 -1.5)
                 (lineto  ,(* a-length -1) -1.5)
                 (lineto  -1.5 -1.0)
                 (lineto  -1.5 -2.0)
                 (lineto   0.0  0.0)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto  0.0  0.0)
                 (lineto -1.5  2.0)
                 (lineto -1.5  1.0)
                 (lineto ,(* a-length -1)  1.5)
                 (lineto ,(* a-length -1) -1.5)
                 (lineto -1.5 -1.0)
                 (lineto -1.5 -2.0)
                 (lineto  0.0  0.0)
                 (closepath)))))))
     
      ((and (string=? type "large")(and (eq? axis X)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.0  0.0)
                 (lineto   1.5  2.0)
                 (lineto   1.5  1.0)
                 (lineto  ,a-length  1.5)
                 (rlineto -1.0 -1.5)
                 (rlineto  1.0 -1.5)
                 (lineto  ,a-length -1.5)
                 (lineto   1.5 -1.0)
                 (lineto   1.5 -2.0)
                 (lineto   0.0  0.0)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto  0.0  0.0)
                 (lineto  1.5  2.0)
                 (lineto  1.5  1.0)
                 (lineto ,a-length  1.5)
                 (lineto ,a-length -1.5)
                 (lineto  1.5 -1.0)
                 (lineto  1.5 -2.0)
                 (lineto  0.0  0.0)
                 (closepath)))))))
      
      ((and (string=? type "large")(and (eq? axis Y)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.0  0.0)
                 (lineto   2.0 -1.5)
                 (lineto   1.0 -1.5)
                 (lineto   1.5 ,(* a-length -1))
                 (rlineto -1.5  1.0)
                 (rlineto -1.5 -1.0)
                 (lineto  -1.5 ,(* a-length -1))
                 (lineto  -1.0 -1.5)
                 (lineto  -2.0 -1.5)
                 (lineto   0.0  0.0)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto  0.0  0.0)
                 (lineto  2.0 -1.5)
                 (lineto  1.0 -1.5)
                 (lineto  1.5 ,(* a-length -1))
                 (lineto -1.5 ,(* a-length -1))
                 (lineto -1.0 -1.5)
                 (lineto -2.0 -1.5)
                 (lineto  0.0  0.0)
                 (closepath)))))))
     
      ((and (string=? type "large")(and (eq? axis Y)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.0  0.0)
                 (lineto   2.0  1.5)
                 (lineto   1.0  1.5)
                 (lineto   1.5 ,a-length)
                 (rlineto -1.5 -1.0)
                 (rlineto -1.5  1.0)
                 (lineto  -1.5 ,a-length)
                 (lineto  -1.0  1.5)
                 (lineto  -2.0  1.5)
                 (lineto   0.0  0.0)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto  0.0  0.0)
                 (lineto  2.0  1.5)
                 (lineto  1.0  1.5)
                 (lineto  1.5 ,a-length)
                 (lineto -1.5 ,a-length)
                 (lineto -1.0  1.5)
                 (lineto -2.0  1.5)
                 (lineto  0.0  0.0)
                 (closepath)))))))
      
;; "wings" optimal thickness #0.1
      ((and (string=? type "wings")(and (eq? axis X)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (lineto  -1.40  1.40)
                 (lineto  -2.00  1.40)
                 (lineto  -0.87  0.23)
                 (lineto  ,(* (- a-length 3.88) -1)  0.22)
                 (lineto  ,(* (- a-length 2.80) -1)  1.40)
                 (lineto  ,(* (- a-length 2.20) -1)  1.40)
                 (lineto  ,(* (- a-length 3.40) -1)  0.00)
                 (lineto  ,(* (- a-length 2.20) -1) -1.40)
                 (lineto  ,(* (- a-length 2.80) -1) -1.40)
                 (lineto  ,(* (- a-length 3.88) -1) -0.22)
                 (lineto  ,(* (- a-length 3.88) -1) -0.22)
                 (lineto  -0.87 -0.22)
                 (lineto  -2.0 -1.40)
                 (lineto  -1.40 -1.40)
                 (closepath)
                 (moveto  ,(* (- a-length 2.90) -1)  0.00)
                 (rlineto -1.20  1.40)
                 (rlineto -0.60  0.00)
                 (rlineto  1.20 -1.40)
                 (rlineto -1.20 -1.40)
                 (rlineto  0.60  0.00)
                 (rlineto  1.20  1.40)
                 (rmoveto -1.10  0.00)
                 (rlineto -1.20  1.40)
                 (rlineto -0.60  0.00)
                 (rlineto  1.20 -1.40)
                 (rlineto -1.20 -1.40)
                 (rlineto  0.60  0.00)
                 (rlineto  1.20  1.40)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (lineto  -1.40  1.40)
                 (lineto  -2.00  1.40)
                 (lineto  -0.87  0.23)
                 (lineto  ,(* a-length -1)  0.22)
                 (lineto  ,(* a-length -1) -0.22)
                 (lineto  -0.87 -0.22)
                 (lineto  -2.00 -1.40)
                 (lineto  -1.40 -1.40)
                 (closepath)))))))
     
      ((and (string=? type "wings")(and (eq? axis X)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto  -0.00  0.00)
                 (lineto   1.40  1.40)
                 (lineto   2.00  1.40)
                 (lineto   0.87  0.23)
                 (lineto  ,(- a-length 3.88)  0.22)
                 (lineto  ,(- a-length 2.80)  1.40)
                 (lineto  ,(- a-length 2.20)  1.40)
                 (lineto  ,(- a-length 3.40)  0.00)
                 (lineto  ,(- a-length 2.20) -1.40)
                 (lineto  ,(- a-length 2.80) -1.40)
                 (lineto  ,(- a-length 3.88) -0.22)
                 (lineto  ,(- a-length 3.88) -0.22)
                 (lineto   0.87 -0.22)
                 (lineto   2.0 -1.40)
                 (lineto   1.40 -1.40)
                 (closepath)
                 (moveto  ,(- a-length 2.90)  0.00)
                 (rlineto  1.20  1.40)
                 (rlineto  0.60  0.00)
                 (rlineto -1.20 -1.40)
                 (rlineto  1.20 -1.40)
                 (rlineto -0.60  0.00)
                 (rlineto -1.20  1.40)
                 (rmoveto  1.10  0.00)
                 (rlineto  1.20  1.40)
                 (rlineto  0.60  0.00)
                 (rlineto -1.20 -1.40)
                 (rlineto  1.20 -1.40)
                 (rlineto -0.60  0.00)
                 (rlineto -1.20  1.40)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (lineto   1.40  1.40)
                 (lineto   2.00  1.40)
                 (lineto   0.87  0.23)
                 (lineto  ,a-length  0.22)
                 (lineto  ,a-length -0.22)
                 (lineto   0.87 -0.22)
                 (lineto   2.00 -1.40)
                 (lineto   1.40 -1.40)
                 (closepath)))))))
      
      ((and (string=? type "wings")(and (eq? axis Y)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto  0.00  0.00)
                 (lineto  1.40 -1.40)
                 (lineto  1.40 -2.00)
                 (lineto  0.23 -0.87)
                 (lineto  0.22 ,(* (- a-length 3.88) -1))
                 (lineto  1.40 ,(* (- a-length 2.80) -1))
                 (lineto  1.40 ,(* (- a-length 2.20) -1))
                 (lineto  0.00 ,(* (- a-length 3.40) -1))
                 (lineto -1.40 ,(* (- a-length 2.20) -1))
                 (lineto -1.40 ,(* (- a-length 2.80) -1))
                 (lineto -0.22 ,(* (- a-length 3.88) -1))
                 (lineto -0.22 ,(* (- a-length 3.88) -1))
                 (lineto -0.22 -0.87)
                 (lineto -1.40 -2.0)
                 (lineto -1.40 -1.40)
                 (closepath)
                 (moveto   0.00 ,(* (- a-length 2.90) -1))
                 (rlineto  1.40 -1.20)
                 (rlineto  0.00 -0.60)
                 (rlineto -1.40  1.20)
                 (rlineto -1.40 -1.20)
                 (rlineto  0.00  0.60)
                 (rlineto  1.40  1.20)
                 (rmoveto  0.00 -1.10)
                 (rlineto  1.40 -1.20)
                 (rlineto  0.00 -0.60)
                 (rlineto -1.40  1.20)
                 (rlineto -1.40 -1.20)
                 (rlineto  0.00  0.60)
                 (rlineto  1.40  1.20)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto  0.00  0.00)
                 (lineto  1.40 -1.40)
                 (lineto  1.40 -2.00)
                 (lineto  0.23 -0.87)
                 (lineto  0.22 ,(* a-length -1))
                 (lineto -0.22 ,(* a-length -1))
                 (lineto -0.22 -0.87)
                 (lineto -1.40 -2.00)
                 (lineto -1.40 -1.40)
                 (closepath)))))))
     
      ((and (string=? type "wings")(and (eq? axis Y)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto  0.00  0.00)
                 (lineto  1.40  1.40)
                 (lineto  1.40  2.00)
                 (lineto  0.23  0.87)
                 (lineto  0.22 ,(- a-length 3.88))
                 (lineto  1.40 ,(- a-length 2.80))
                 (lineto  1.40 ,(- a-length 2.20))
                 (lineto  0.00 ,(- a-length 3.40))
                 (lineto -1.40 ,(- a-length 2.20))
                 (lineto -1.40 ,(- a-length 2.80))
                 (lineto -0.22 ,(- a-length 3.88))
                 (lineto -0.22 ,(- a-length 3.88))
                 (lineto -0.22  0.87)
                 (lineto -1.40  2.00)
                 (lineto -1.40  1.40)
                 (closepath)
                 (moveto   0.00 ,(- a-length 2.90))
                 (rlineto  1.40  1.20)
                 (rlineto  0.00  0.60)
                 (rlineto -1.40 -1.20)
                 (rlineto -1.40  1.20)
                 (rlineto  0.00 -0.60)
                 (rlineto  1.40 -1.20)
                 (rmoveto  0.00  1.10)
                 (rlineto  1.40  1.20)
                 (rlineto  0.00  0.60)
                 (rlineto -1.40 -1.20)
                 (rlineto -1.40  1.20)
                 (rlineto  0.00 -0.60)
                 (rlineto  1.40 -1.20)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #f)
              (#:path boldness
               `((moveto  0.00  0.00)
                 (lineto  1.40  1.40)
                 (lineto  1.40  2.00)
                 (lineto  0.23  0.87)
                 (lineto  0.22 ,a-length)
                 (lineto -0.22 ,a-length)
                 (lineto -0.22  0.87)
                 (lineto -1.40  2.00)
                 (lineto -1.40  1.40)
                 (closepath))))))) 
        
;; "relief" optimal thickness #0.01
      ((and (string=? type "relief")(and (eq? axis X)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (lineto  -3.00  1.00)
                 (lineto  -1.50  0.00)
                 (lineto  -2.15  0.00)
                 (curveto -2.15 -0.15 -2.30 -0.15 -2.30 -0.15)
                 (lineto  -3.00 -1.00)
                 (lineto  -1.50  0.00)
                 (closepath)
                 (lineto  -3.00 -1.00)
                 (lineto   0.00  0.00)
                 (closepath)
                 (moveto  -2.00  0.00)
                 (lineto  ,(* (- a-length 0.30) -1)  0.00)
                 (lineto  ,(* (- a-length 0.30) -1)  0.15)
                 (rlineto  0.70  0.00)
                 (rlineto -1.00  0.85)
                 (rlineto  0.50 -0.85)
                 (rlineto -0.50  0.85)
                 (rlineto  2.50  0.00)
                 (rlineto  1.00 -0.85)
                 (lineto  -2.30  0.15)
                 (lineto  -3.00  1.00)
                 (lineto  -2.30  0.15)
                 (curveto -2.20  0.15 -2.15  0.10 -2.15  0.00)
                 (closepath)
                 (moveto  -2.30 -0.15)
                 (lineto  ,(* (- a-length 0.30) -1) -0.15)
                 (lineto  ,(* (- a-length 0.30) -1)  0.00)
                 (lineto  ,(* (- a-length 0.30) -1) -0.15)
                 (rmoveto  0.70  0.00)
                 (rlineto -1.00 -0.85)
                 (rlineto  0.50  0.85)
                 (rlineto  0.50  0.00)
                 (rmoveto -1.00 -0.85)
                 (rlineto  2.50  0.00)
                 (rlineto  1.00  0.85)
                 (rlineto -1.00 -0.85)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (lineto  -3.00  1.00)
                 (lineto  -1.50  0.00)
                 (lineto  -2.15  0.00)
                 (curveto -2.15 -0.15 -2.30 -0.15  -2.30 -0.15)
                 (lineto  -3.00 -1.00)
                 (lineto  -1.50  0.00)
                 (closepath)
                 (lineto  -3.00 -1.00)
                 (lineto   0.00  0.00)
                 (closepath)
                 (moveto  -2.00  0.00)
                 (lineto ,(* (- a-length 0.30) -1)  0.00)
                 (lineto ,(* (- a-length 0.30) -1)  0.15)
                 (lineto  -2.30  0.15)
                 (lineto  -3.00  1.00)
                 (lineto  -2.30  0.15)
                 (curveto -2.20  0.15  -2.15  0.10  -2.15  0.00)
                 (closepath)
                 (moveto  -2.30 -0.15)
                 (lineto ,(* (- a-length 0.30) -1) -0.15)
                 (lineto ,(* (- a-length 0.30) -1)  0.00)
                 (lineto ,(* (- a-length 0.30) -1) -0.15)
                 (closepath)))))))
     
      ((and (string=? type "relief")(and (eq? axis X)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (lineto   3.00  1.00)
                 (lineto   1.50  0.00)
                 (lineto   2.15  0.00)
                 (curveto  2.15 -0.15  2.30 -0.15 2.30 -0.15)
                 (lineto   3.00 -1.00)
                 (lineto   1.50  0.00)
                 (closepath)
                 (lineto   3.00 -1.00)
                 (lineto   0.00  0.00)
                 (closepath)
                 (moveto   2.00  0.00)
                 (lineto  ,(- a-length 0.30)  0.00)
                 (lineto  ,(- a-length 0.30)  0.15)
                 (rlineto -0.70  0.00)
                 (rlineto  1.00  0.85)
                 (rlineto -0.50 -0.85)
                 (rlineto  0.50  0.85)
                 (rlineto -2.50  0.00)
                 (rlineto -1.00 -0.85)
                 (lineto   2.30  0.15)
                 (lineto   3.00  1.00)
                 (lineto   2.30  0.15)
                 (curveto  2.20  0.15  2.15  0.10  2.15  0.00)
                 (closepath)
                 (moveto   2.30 -0.15)
                 (lineto  ,(- a-length 0.30) -0.15)
                 (lineto  ,(- a-length 0.30)  0.00) 
                 (lineto  ,(- a-length 0.30) -0.15)
                 (rmoveto -0.70  0.00)
                 (rlineto  1.00 -0.85)
                 (rlineto -0.50  0.85)
                 (rlineto -0.50  0.00)
                 (rmoveto  1.00 -0.85)
                 (rlineto -2.50  0.00)
                 (rlineto -1.00  0.85)
                 (rlineto  1.00 -0.85)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto  0.00  0.00)
                 (lineto  3.00  1.00)
                 (lineto  1.50  0.00)
                 (lineto  2.15  0.00)
                 (curveto 2.15 -0.15  2.30 -0.15 2.30 -0.15)
                 (lineto  3.00 -1.00)
                 (lineto  1.50  0.00)
                 (closepath)
                 (lineto  3.00 -1.00)
                 (lineto  0.00  0.00)
                 (closepath)
                 (moveto  2.00  0.00)
                 (lineto ,a-length  0.00)
                 (lineto ,a-length  0.15)
                 (lineto  2.30  0.15)
                 (lineto  3.00  1.00)
                 (lineto  2.30  0.15)
                 (curveto 2.20  0.15  2.15  0.10  2.15  0.00)
                 (closepath)
                 (moveto  2.30 -0.15)
                 (lineto ,a-length -0.15)
                 (lineto ,a-length  0.00)
                 (lineto ,a-length -0.15)
                 (closepath)))))))
      
      ((and (string=? type "relief")(and (eq? axis Y)(eq? direction 1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.00   0.00)
                 (lineto   1.00  -3.00)
                 (lineto   0.00  -1.50)
                 (lineto   0.00  -2.15)
                 (curveto -0.15  -2.15 -0.15 -2.30 -0.15 -2.30)
                 (lineto  -1.00  -3.00)
                 (lineto   0.00  -1.50)
                 (closepath)
                 (lineto  -1.00 -3.00)
                 (lineto   0.00  0.00)
                 (closepath)
                 (moveto   0.00 -2.00)
                 (lineto   0.00 ,(* (- a-length 0.30) -1))
                 (lineto   0.15 ,(* (- a-length 0.30) -1))
                 (rlineto  0.00  0.70)
                 (rlineto  0.85 -1.00)
                 (rlineto -0.85  0.50)
                 (rlineto  0.85 -0.50)
                 (rlineto  0.00  2.50)
                 (rlineto -0.85  1.00)
                 (lineto   0.15 -2.30)
                 (lineto   1.00 -3.00)
                 (lineto   0.15 -2.30)
                 (curveto  0.15 -2.20  0.10 -2.15  0.00 -2.15)
                 (closepath)
                 (moveto  -0.15  -2.30)
                 (lineto  -0.15  ,(* (- a-length 0.30) -1))
                 (lineto   0.00  ,(* (- a-length 0.30) -1))
                 (lineto  -0.15  ,(* (- a-length 0.30) -1))
                 (rmoveto  0.00  0.70)
                 (rlineto -0.85 -1.00)
                 (rlineto  0.85  0.50)
                 (rlineto  0.00  0.50)
                 (rmoveto -0.85 -1.00)
                 (rlineto  0.00  2.50)
                 (rlineto  0.85  1.00)
                 (rlineto -0.85 -1.00)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (lineto   1.00 -3.00)
                 (lineto   0.00 -1.50)
                 (lineto   0.00 -2.15)
                 (curveto -0.15 -2.15 -0.15 -2.30 -0.15  -2.30)
                 (lineto  -1.00 -3.00)
                 (lineto   0.00 -1.50)
                 (closepath)
                 (lineto  -1.00 -3.00)
                 (lineto   0.00  0.00)
                 (closepath)
                 (moveto   0.00 -2.00)
                 (lineto   0.00 ,(* (- a-length 0.30) -1))
                 (lineto   0.15 ,(* (- a-length 0.30) -1))
                 (lineto   0.15 -2.30)
                 (lineto   1.00 -3.00)
                 (lineto   0.15 -2.30)
                 (curveto  0.15 -2.20  0.10  -2.15  0.00  -2.15)
                 (closepath)
                 (moveto  -0.15 -2.30)
                 (lineto  -0.15 ,(* (- a-length 0.30) -1))
                 (lineto   0.00 ,(* (- a-length 0.30) -1))
                 (lineto  -0.15 ,(* (- a-length 0.30) -1))
                 (closepath)))))))
     
      ((and (string=? type "relief")(and (eq? axis Y)(eq? direction -1)))
       (if (eq? fletching fletching?)
           (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.00  -0.00)
                 (lineto   1.00   3.00)
                 (lineto   0.00   1.50)
                 (lineto   0.00   2.15)
                 (curveto -0.15   2.15 -0.15  2.30 -0.15  2.30)
                 (lineto  -1.00   3.00)
                 (lineto   0.00   1.50)
                 (closepath)
                 (lineto  -1.00  3.00)
                 (lineto   0.00  0.00)
                 (closepath)
                 (moveto   0.00  2.00)
                 (lineto   0.00 ,(- a-length 0.30))
                 (lineto   0.15 ,(- a-length 0.30))
                 (rlineto  0.00 -0.70)
                 (rlineto  0.85  1.00)
                 (rlineto -0.85 -0.50)
                 (rlineto  0.85  0.50)
                 (rlineto  0.00 -2.50)
                 (rlineto -0.85 -1.00)
                 (lineto   0.15  2.30)
                 (lineto   1.00  3.00)
                 (lineto   0.15  2.30)
                 (curveto  0.15  2.20  0.10  2.15  0.00  2.15)
                 (closepath)
                 (moveto  -0.15   2.30)
                 (lineto  -0.15  ,(- a-length 0.30))
                 (lineto   0.00  ,(- a-length 0.30))
                 (lineto  -0.15  ,(- a-length 0.30))
                 (rmoveto  0.00 -0.70)
                 (rlineto -0.85  1.00)
                 (rlineto  0.85 -0.50)
                 (rlineto  0.00 -0.50)
                 (rmoveto -0.85  1.00)
                 (rlineto  0.00 -2.50)
                 (rlineto  0.85 -1.00)
                 (rlineto -0.85  1.00)
                 (closepath)))))
            (markup 
             (#:override (cons (quote filled) #t)
              (#:path boldness
               `((moveto   0.00  0.00)
                 (lineto   1.00  3.00)
                 (lineto   0.00  1.50)
                 (lineto   0.00  2.15)
                 (curveto -0.15  2.15 -0.15  2.30 -0.15  2.30)
                 (lineto  -1.00  3.00)
                 (lineto   0.00  1.50)
                 (closepath)
                 (lineto  -1.00  3.00)
                 (lineto   0.00  0.00)
                 (closepath)
                 (moveto   0.00  2.00)
                 (lineto   0.00 ,(- a-length 0.30))
                 (lineto   0.15 ,(- a-length 0.30))
                 (lineto   0.15  2.30)
                 (lineto   1.00  3.00)
                 (lineto   0.15  2.30)
                 (curveto  0.15  2.20  0.10   2.15  0.00  2.15)
                 (closepath)
                 (moveto  -0.15  2.30)
                 (lineto  -0.15 ,(- a-length 0.30))
                 (lineto   0.00 ,(- a-length 0.30))
                 (lineto  -0.15 ,(- a-length 0.30))
                 (closepath)))))))
          
      (else (ly:error "Arrows' parameter(s) do not fit")))))

%%% end "arrows.ily %%%


%%%% ARROWS LIST %%%%%

%\markup
%\override #'(baseline-skip . 3)
%\column { \italic "Arrows' type list:" \null }

\markup {
  \center-column {
    \italic { "ï¿½openï¿½" "(bold. #0.14)" }
    \vspace #0.5
    \concat
    \scale #'(1.5 . 1.5)
    \vcenter {
      \arrow #"open" ##t #Y #UP #10 #0.14
      \hspace #1
      \arrow #"open" ##f #Y #DOWN #10 #0.14      
    }
  }
  \hspace #1
  \center-column {
    \italic { "ï¿½closedï¿½" "(bold. #0.07)" }
    \vspace #0.5
    \concat
    \scale #'(1.5 . 1.5)
    \vcenter {
      \arrow #"closed" ##t #Y #UP #10 #0.07
      \hspace #1
      \arrow #"closed" ##f #Y #DOWN #10 #0.07      
    }
  }
  \hspace #1
  \center-column {
    \italic { "ï¿½huronï¿½" "(bold. #0.15)" }
    \vspace #0.5
    \concat
    \scale #'(1.5 . 1.5)
    \vcenter {
      \arrow #"huron" ##t #Y #UP #10 #0.15
      \hspace #1
      \arrow #"huron" ##f #Y #DOWN #10 #0.15      
    }
  }
  \hspace #1
  \center-column {
    \italic { "ï¿½longï¿½" "(bold. #0.07)" }
    \vspace #0.5
    \concat
    \scale #'(1.5 . 1.5)
    \vcenter {
      \arrow #"long" ##t #Y #UP #10 #0.07
      \hspace #1
      \arrow #"long" ##f #Y #DOWN #10 #0.07      
    }
  }  
  \hspace #1
  \center-column {
    \italic { "ï¿½largeï¿½" "(bold. #0.07)" }
    \vspace #0.5
    \concat
    \scale #'(1.5 . 1.5)
    \vcenter {
      \arrow #"large" ##t #Y #UP #10 #0.07
      \hspace #1
      \arrow #"large" ##f #Y #DOWN #10 #0.07      
    }
  }    
  \hspace #1
  \center-column {
    \italic { "ï¿½wingsï¿½" "(bold. #0.10)" }
    \vspace #0.5
    \concat
    \scale #'(1.5 . 1.5)
    \vcenter {
      \arrow #"wings" ##t #Y #UP #10 #0.1
      \hspace #1
      \arrow #"wings" ##f #Y #DOWN #10 #0.1  
    }
  }    
  \hspace #1
  \center-column {
    \italic { "ï¿½reliefï¿½" "(bold. #0.03)" }
    \vspace #0.5
    \concat
    \scale #'(1.5 . 1.5)
    \vcenter {
      \arrow #"relief" ##t #Y #UP #10 #0.03
      \hspace #1
      \arrow #"relief" ##f #Y #DOWN #10 #0.03      
    }
  \vspace #2
  }    
}

%%%%% TESTS %%%%%%%

\markup
\override #'(baseline-skip . 3)
\column { \italic "Use case examples:" \null }


%% As stroke indication:
ri = \rightHandFinger #2
ru = \rightHandFinger\markup\arrow #"huron" ##f #Y #UP #2 #0.15

\score {
  \new Staff {
    \clef "G_8"
    \time 3/4
    \key d\minor
    <d' f' a^\ri^\ru>4
    <f' a' d''^\ru>
    <d' f' a'^\ru>
    <a cis' e' a'^\ru>8
  }
  \layout {
    indent = 0 
  }
  \header {
    piece = \markup \bold "Fandango"
  }
}

%% As instrument name, as tempo warning
\new Staff 
  \with { instrumentName = \markup\arrow #"large" ##t #X #RIGHT #7 #0.07 }
  \relative c' { 
    \tempo 
    \markup {
      \concat {
        \raise #1
        \scale #'(1.2 . 0.8)
        \with-color #red 
        \arrow #"wings" ##f #X #RIGHT #5 #0.3 " ("
        \smaller \general-align #Y #DOWN \note {16.} #1
        " = "
        \smaller \general-align #Y #DOWN \note {8} #1
        ")"
      }
    } 
    c8 d e f g a b c 
  }
  
%% As indication, as 'balloonGrobText alternative:
\score {
  \new Staff {
    \clef "G_8"
    \stemDown
    e,2 \textLengthOff e_~
    ^\markup {
        \with-dimensions #'(0 . 0) #'(0 . 0)
        \rotate #150 
        \translate #'(-1 . 0)
        \arrow #"open" ##f #Y #UP #7.5 #0.2
        \tiny\italic { "This is an E" }
    }
    e
  }
  \layout {
    indent = 0 
  }
}

%% Put some space here:
\markup \vspace #1
%%%%%%%%%%%%%%%%%%%%%%

%% As conducting signs:
\markup 
\scale #'(0.7 . 0.7)
{
  %% 4 beats
  \combine
  \concat { 
    \column {
      \lower #1 \number "2 "
      \translate #'(1 . 1)
      \rotate #55
      \with-dimensions #'(0 . 0) #'(0 . 0)
      \arrow #"long" ##t #Y #UP #12 #0.07 
    } 
    \arrow #"long" ##t #X #RIGHT #21 #0.07  
    \lower #1
    \number " 3"
  }
  \translate #'(11.5 . 10)
  \center-column { 
    \column {
      \lower #1 \number "4"
      \translate #'(2 . 2)
      \rotate #55
      \with-dimensions #'(0 . 0) #'(0 . 0)
      \arrow #"long" ##t #Y #UP #12 #0.07 
    } 
    \vspace #.3 
    \arrow #"long" ##t #Y #DOWN #17 #0.07 
    \number "1" 
  }
  
%% Put some space here:
\hspace #4
%%%%%%%%%%%%%%%%%%%%%%  

  %% 3 beats
  \vcenter {
    \center-column { 
      \number "3" \vspace #.5 
      \arrow #"long" ##t #Y #DOWN #17 #0.07 
      \number "1" 
    }
    \center-column { 
      \rotate #-30 \arrow #"long" ##t #X #LEFT #17 #0.07
      \rotate #30 \arrow #"long" ##t #X #RIGHT #17 #0.07
    }
  }
  \lower #1 \number " 2"
}

%% Put some space here:
\markup \vspace #1
%%%%%%%%%%%%%%%%%%%%%%

%% In titles as markups
\score {
  \relative c' { c4 e g }
  \layout {
    indent = 0 
  }
  \header {
    piece = \markup\override #'(baseline-skip . 1.5)\center-column {
      \scale #'(2 . 1) \smallCaps "ArrowS"
      \arrow #"relief" ##t #X #RIGHT #18 #0.03
    }
  }
}

%% As split staff indication :
\markup\column { 
  \line { 
    See also: 
    \italic %\with-url "http://lsr.di.unimi.it/LSR/Item?id=650"
    \concat { "ï¿½Adding indicators to staves which get split after a breakï¿½" }
  }
  \line { 
    and see how 
    \typewriter "'arrow-at-angle" 
    could be re-written with 
    \typewriter "'arrow" 
    instead of \typewriter "'arrow-head."
  }
  \line { This will help removing the ungainly space between arrows head and arrows shaft. }
}

%% see http://lsr.di.unimi.it/LSR/Item?id=650
%% 'arrow-at-angle can be written as:
#(define-markup-command (arrow-at-angle layout props angle-deg length fill)
   (number? number? boolean?)
   (let* ((PI-OVER-180 (/ (atan 1 1) 34))
          (degrees->radians (lambda (degrees) (* degrees PI-OVER-180)))
          (angle-rad (degrees->radians angle-deg))
          (target-x (* length (cos angle-rad)))
          (target-y (* length (sin angle-rad))))
     (interpret-markup layout props
       (markup
        #:translate (cons (/ target-x 2) (/ target-y 2))
        #:rotate angle-deg
        #:translate (cons (/ length 1.3) 0)
        #:arrow "closed" #f 0 1 3.4 0.07 ))))

%%%% SNIPPET END %%%%
