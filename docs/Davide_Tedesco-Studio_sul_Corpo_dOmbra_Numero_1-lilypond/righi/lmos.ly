lmo_uno=  {
  \override Staff.StaffSymbol.line-count = #1
   \override Staff.StaffSymbol.line-positions = #'(0)
  %\override StaffSymbol.thickness = #(magstep 10)
   \hide Clef
     \numericTimeSignature
     %\override TextScript.extra-offset = #'(0 . 11.1)
     %\override Glissando.thickness = #'2
     \hide Clef
       \numericTimeSignature
             %spazi lasciati per esteso e non raggruppati per poter inserire tape all'occorrenza in ogni posizione

   \hideNotes %<<{
      
      %s1. s4
      %s1 s2.
      %s1 s2 s4 
     \gliss #'(0 . -5) #' 0.4 \once \override TextSpanner.bound-details.right.padding = #.5
      d,1.\<\ppp^\markup{\box \column{\line{Live Multi Oscillator \box 1  30 Hz}
                                                         %\line {\box 2 70 Hz}
                                                         %\line{\box 3  90 Hz}
                                                         %\line {\box 4 100 Hz}
                                                          }}%_\markup{ \box 1  60 Hz}
        \Stsp
    g'4\p
        
     d,1 g'2.
     s1 s2^\markup{\translate #'(-1 . 2) \box \column{\line{35 Hz}}}s4 \Stpsp
     
    
      \myBreak
     
     \gliss #'(0 . -1.6) #' 0.4 \once \override TextSpanner.bound-details.right.padding = #-1.5  d,1^\markup{\translate #'(-1 . 2) \box \column{\line{35 Hz}}} \Stsp
     s1 
     s1
     s2.^\markup{\translate #'(-1 . 2) \box \column{\line{40 Hz}}}\mf s8 s16 s16\Stpsp
     
      \myBreak
      
     \gliss #'(0 . -1.6) #' 0.4  \once \override TextSpanner.bound-details.right.padding = #-4.5 s1^\markup{\translate #'(-1 . 2) \box \column{\line{\box{1}\box{2 }40 Hz}}} \Stsp
     s1
     s1
     s4 s4\f s4 s8 s16 s16^\markup{\translate #'(-6 . 2) \box \column{\line{45 Hz}}} \Stpsp
     
      \myBreak
      
     \gliss #'(0 . -1.6) #' 2 \once \override TextSpanner.bound-details.right.padding = #0.6 s1^\markup{\translate #'(-1 . 2) \box \column{\line{45 Hz}}}  \Stsp
     s1
     s1
     s1
     s1
     s4  s4^\markup{\translate #'(-1 . 2) \box \column{\line{92 Hz}}}  s4\! s4\Stpsp
     
      \myBreak
      
      \gliss #'(0 . 0) #' 0 \once \override TextSpanner.bound-details.right.padding = #0.6 s1.^\markup{\translate #'(-1 . 2) \box \column{\line{92 Hz}}} \Stsp
     s1.
     s1.
     s1.
     s1.\ff
     
  \myBreak
      
     s1.
     s1.
     s1.     
     \alniente s1\>^\markup{\translate #'(-1 . 2) \box \column{\line{92 Hz}}} \Stpsp s4\! s4
     
      \myBreak
      
     s1
     s1
     s1
     s1
     s1
     
      \myBreak
      
     s1
     s1
     s1
     
      \myBreak
      
     s1
     s1
     s1
     s1
     s1
     
      \myBreak
      
     \gliss #'(0 . 0) #' 2 \once \override TextSpanner.bound-details.right.padding = #0 s1.^\markup{\box \column{\line{\box{1}\box{2 }\box{3 } 139 Hz}}}\<\pp \Stsp
     s1.
     s1.
     
      \myBreak
      
     s1.
     s1.\f
     
      \myBreak
      
     s1.\f
     s1.
     s1.
     
      \myBreak
      
     s1.\f
     s1.
     s1.
     
     \myBreak
     \once \override TextSpanner.bound-details.right.padding = #2
     
     s1.\f^\markup{\box \column{\line{\box{1}\box{2 }\box{3 } \box{4 } 207 Hz}}} s4
     s1. s4
     
     \myBreak
     
     s1.\f s4
     s1. s4
     s1. s4
     
     \myBreak
     
     s1.\f s4
     s1. s4
     
     \myBreak
     
     s1\f s4
     s1 s4
     
     \myBreak
     
     s2\f s8
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     
     \myBreak
     
     s1\f s2.^\markup{\translate #'(-1 . 2) \box \column{\line{487 Hz}}}
     s1 s2.
     
     \myBreak
     
     s1\f s2.
     s1 s2.
     
     \myBreak
     
     s1\f s2.
     s4 s4 s4 s8 s8 s2.
     
     \myBreak
     
     s1\f s2.
     \alniente s1\>^\markup{\translate #'(-1 . 2) \box \column{\line{ 987 Hz}}} s4\!  \Stpsp  s4 s4       
     
     \myBreak
     
     s1 s2.
     s1 s2.
          
     \myBreak
     
     s2  \gliss #'(0 . 5) #' 0 \once \override TextSpanner.bound-details.right.padding = #0  s2^\markup{\translate #'(-1 . 8) \box \column{\line{\box{1}\box{2 }\box{3 } \box{4 } 1046 Hz}}}\<\ppp \Stsp s2. 
     s1 s4 s4 s8 s8\fff \Stpsp
}

whiteNoiseLMO=  {
  \override Staff.StaffSymbol.line-count = #1
   \override Staff.StaffSymbol.line-positions = #'(0 )
  %\override StaffSymbol.thickness = #(magstep 10)
   \hide Clef
     \numericTimeSignature
     %\override TextScript.extra-offset = #'(0 . 11.1)
     %\override Glissando.thickness = #'2
     \hide Clef
       \numericTimeSignature
             %spazi lasciati per esteso e non raggruppati per poter inserire tape all'occorrenza in ogni posizione

   \hideNotes %<<{
   \once \override TextSpanner.bound-details.right.padding = #.5
     
     \guide #'(0 . -4.7) #' 0 d,1.\<\pp^\markup{\box \column{\line{White Noise Live Multi Oscillator: HPF 20 Hz}}}
        \Stsp
        
    g'4\mp
        
     d,1 g'2.
     \alniente \once \override DynamicLineSpanner.staff-padding = #10 s1\> s2\! s4 \Stpsp  
     
    
      \myBreak
    s1 
     s1
     \guide #'(0 . -1.8) #' 0 \once \override TextSpanner.bound-details.right.padding = #0 \alniente s1\<^\markup{\box \column{\line{HPF 20 Hz}}} \Stsp
     s1
     
      \myBreak
      
     s1\f
     s2 \alniente s4\> s8\! s8\Stpsp
     s1
      \alniente \guide #'(0 . -1.2) #' 0 s1\<^\markup{\box \column{\line{HPF 40 Hz}}} \Stsp
     
      \myBreak
      
     s1\f
     s1
     s1
     s1 
     s4 \alniente s4\>^\markup{\box \column{\line{HPF 50 Hz}}} s4\! s4\Stpsp
     s1
     
      \myBreak
      
     s1.
     s2 \alniente \guide #'(0 . -1.2) #' 2 s1\<^\markup{\box \column{\line{HPF 70 Hz}}} \Stsp
     s1.\f
     s1.
     s1.
     
  \myBreak
      
     s1.
     s1.
     s1.    
     s1.
     
      \myBreak
      
     \alniente s1\>
      s4 s4 \Stpsp s4\! s4
     s1
     s1
     s1
     
      \myBreak
      
     s2 \alniente \guide #'(0 . -1.2) #' 2 \once \override TextSpanner.bound-details.right.padding = #1 s2\ppp\<^\markup{\box \column{\line{HPF 80 Hz}}} \Stsp
     s1
     s1
     
      \myBreak
      
     s1
     s1\p
     \alniente s1\>^\markup{\box \column{\line{HPF 93 Hz}}}
     s4\! \Stpsp s4 s4 s4
     s1
     
      \myBreak
      
     \alniente \guide #'(0 . -1.2) #' 2 s1.\<^\markup{\box \column{\line{HPF 100 Hz}}}\ppp \Stsp
     s1.
     s1.
     
      \myBreak
      
     s1.
     s1.\mf
     
      \myBreak
      
     s1.
     s1.
     s1.
     
      \myBreak
      
     s1.
     s1.
     s1.
     
     \myBreak
     
     s1. s4
     s1. s4
     
     \myBreak
     
     s1. s4
     s1. s4
     s1. s4
     
     \myBreak
     
     s1. s4
     s1. s4
     
     \myBreak
     
     s1 s4
     s1 s4
     
     \myBreak
     
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     \alniente s4\>^\markup{\box \column{\line{HPF 392 Hz}}} s4\! \Stpsp  s8 
     
     \myBreak
     
     s4 s4 s4 s4 s2. 
     s1 s2.
     
     \myBreak
     
     s1 s2.
     s1 s2.
     
     \myBreak
     
     s1 s2.
     s4 s4 s4 s8 s8 s2.
     
     \myBreak
     
     s2. \alniente \guide #'(0 . -1.2) #' 3 s1\<^\markup{\box \column{\line{HPF 439 Hz}}} \Stsp
     s1 s2.^\markup{\box \column{\line{HPF 987 Hz}}}\f     
     
     \myBreak
     
     s1\f s2.
     s1 \alniente s4\>^\markup{\box \column{\line{HPF 999 Hz}}} s4\! \Stpsp s4
          
     \myBreak
     
     s1 s2.
     s1 s2.
}

