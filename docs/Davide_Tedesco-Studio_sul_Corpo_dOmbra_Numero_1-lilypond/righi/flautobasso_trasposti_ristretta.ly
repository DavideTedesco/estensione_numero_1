%===================================
%============PERSONALIZZAZIONI PER LA SCENA
%===================================
bins = \markup {
            \column {
    \line {\rounded-box "Buio in sala"}
            }
}

lsp = \markup {
            \column { %\translate #'(+5 . 3)
    \line {\rounded-box "Luce graduale sui pannelli"}
    
            }
}

manill = \markup {\rounded-box 
            \column {
    \line {La mano }
    \line{viene illuminata}
    \line{velocemente}
}}

manluoff = \markup {\rounded-box 
            \column {
    \line {Luce mano }
    \line{viene spenta}
    \line{lentamente}
}}

lucvecon = \markup {\rounded-box 
            \column {
    \line {Luce accesa }
    \line{velocemente}
    \line{che illumina}
    \line{tutto il palco}
}}
%===================================
%=============PERSONALIZZAZIONI PER FLAUTO
%===================================
erre = \markup{\column{
                                      \line{Facendo entrare la}
                                      \line{ R progressivamente}
               }			
               }
               

tongueram = \markup{\huge "t.r."}%https://www.flutexpansions.com/tongue-ram
pizzicato = \markup{ \column{ \line{pizz.}}}%https://www.flutexpansions.com/tongue-ram
              
tram = \tweak style #'triangle \etc %https://lsr.di.unimi.it/LSR/Item?id=485

fancy-gliss = %https://lsr.di.unimi.it/LSR/Item?id=1066
#(define-music-function (pts-list)(list?)
   
#{
 \once \override Glissando.after-line-breaking =
 
  #(lambda (grob)
    (let ((stil (ly:line-spanner::print grob)))
     (if (ly:stencil? stil)
         (let* 
           ((left-bound-info (ly:grob-property grob 'left-bound-info))
            (left-bound (ly:spanner-bound grob LEFT))
            (y-off (assoc-get 'Y left-bound-info))
            (padding (assoc-get 'padding left-bound-info))
            (note-column (ly:grob-parent left-bound X))
            (note-heads (ly:grob-object note-column 'note-heads))
            (ext-X 
              (if (null? note-heads)
                  '(0 . 0)
                  (ly:relative-group-extent note-heads grob X)))
            (dot-column (ly:note-column-dot-column note-column))
            (dots 
              (if (null? dot-column)
                  '()
                  (ly:grob-object dot-column 'dots)))
            (dots-ext-X 
              (if (null? dots)
                  '(0 . 0)
                  (ly:relative-group-extent dots grob X)))
            (factor 
              (/ (interval-length (ly:stencil-extent stil X))
                 (car (take-right (last pts-list) 2))))
            (new-stil
              (make-connected-path-stencil 
                (map
                  (lambda (e)
                    (cond ((= (length e) 2)
                           (cons (* (car e) factor) (cdr e)))
                          ((= (length e) 6)
                           (list
                             (* (car e) factor)
                             (cadr e)
                             (* (third e) factor)
                             (fourth e)
                             (* (fifth e) factor)
                             (sixth e)
                            ;; (* (seventh e) factor)-
                            ;; (eighth e)
                             ))
                          (else 
                            (ly:error 
                              "Some element(s) of the given list do not fit"))))   
                  pts-list)
                (layout-line-thickness grob )  ;line-width                     
                1   ;scaling
                1   ;scaling
                #f
                #f)))
         (ly:grob-set-property! grob 'stencil
           (ly:stencil-translate
            new-stil
            (cons (+ (interval-length ext-X) 
                     (interval-length dots-ext-X) 
                     padding) 
                  y-off))))
       (begin
         (ly:warning 
           "Cannot find stencil. Please set 'minimum-length accordingly")
         #f))))
#})

ordinario = \markup{\column{\line{ord.}}}%suono normale

errearrow = \markup {

               \right-align
                 \hspace #2
                 
                \override #'(thickness . 2)
                  \overlay {

        \draw-line #'(7 . 0)

          \translate #'(7 . 0)\arrow-head #X #RIGHT ##t

                  }

  }
  
  %imboccatura aperta-chiusa
  apchius = \markup {

               
                 
                \override #'(thickness . 2)
                  \translate#'(1 . 0)\overlay {
      \draw-circle #0.5 #0.2 ##f
      
         \translate #'(1 . 0)\draw-line #'(14 . 0)
       
          \translate #'(16 . 0)\arrow-head #X #RIGHT ##t
      \translate #'(17 . 0)\draw-circle #0.5 #0.2 ##t

                  }

  }
    %imboccatura chiusa-aperta

  chiusap = \markup {

               
                 
                \override #'(thickness . 2)
                  \translate#'(1 . 0)\overlay {
      \draw-circle #0.5 #0.2 ##t
      
         \translate #'(1 . 0)\draw-line #'(12 . 0)
       
          \translate #'(14 . 0)\arrow-head #X #RIGHT ##t
      \translate #'(15 . 0)\draw-circle #0.5 #0.2 ##f

                  }

  }
  
  
  headPizz        = {
\once \override NoteHead.stencil = #ly:text-interface::print
\once \override NoteHead.text = #(markup #:musicglyph "scripts.sforzato") 
\once \override NoteHead.font-size = #0
  }

  
%=============MULTIFONICI PER FLAUTO
% USEFUL LINKS
%
%https://lilypond.org/doc/v2.23/Documentation/snippets/winds
%https://lsr.di.unimi.it/LSR/Snippet?id=834
%https://lilypond.org/doc/v2.23/Documentation/snippets/winds
%
blu = \markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one two three four five six))
           (lh . (b gis))
           (rh . ())
           )
    }
    
    viola = \markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one two three ))
           (lh . (b ))
           (rh . (cis dis))
           )
    }
    
      verde = \markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one  three four five ))
           (lh . (bes ))
           (rh . (ees))
           )
    }
  
multifonico_la = \markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (two five ))
           (lh . (b))
           (rh . (ees))
           )
    }
  
mantmult = \markup {
        \with-dimensions #'(0 . 0.5) #'(0 . 0)
        \rotate #180
        \translate #'(0.8 . 0.5)
        \arrow #"open" ##f #Y #UP #5 #0.14
        \tiny\italic { "Mantenendo il multifonico fino a qui" }}

jw = \markup{j.w.}

primomultifonico = \markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one two three four five six))
           (lh . (b gis))
           (rh . ())
           )}

secondomultifonico = \markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one two three ))
           (lh . (b ))
           (rh . (cis dis))
           )
    }

terzomultifonico = \markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one  three four five ))
           (lh . (bes ))
           (rh . (ees))
           )
    }    

quartomultifonico = \markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (two five ))
           (lh . (b))
           (rh . (ees))
           )
    }

%===================================
%==================PARTE PER FLAUTO BASSO
%===================================


primavoce = \relative c' 
{   \clef treble
  \numericTimeSignature %http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Displaying-rhythms#Time-signature
  \time 7/4 
   \tempo 4 = 85
   %\mark ^\markup{\box{\column{\line{Partitura scritta in suoni trasposti}}}}
   r16 cis''4.. %cis''4..%^\bins 
     \ppp (cis1)  r8 cis8 \p
   (cis2.) \breathe d4^>\sfz d2. \mf\staccato
   (d2) b4  r8 f8 (f4) r2%^\markup{\circle"1"}
   \time 4/4 
   
   \myBreak
   
   r1
   r1
   r4 r8  <<{dis8   (dis2)
            (dis2)} {s8\f\> s4 s4 s4 s4\p\!} {}>>   r2 
   \myBreak
   
   r8 gis8\mp gis,2.\f 
   (gis2)  r2 
  % \time 4/4 
  
   gis'16 r16 cis,8\p\<
    (e2 bes4 
    bes 2.\f\>) r4\! 
    
    \myBreak
    
   r1 
   r1 
   r1

  <<{fis2^\errearrow^\markup{flt.} (fis:32)} {s4\p\<  s4 s4 s4\ff\!}>> 
  r4. <<{g8:64^\markup{flt.} (g2:32) 
  (g1:32)} \alniente  {s8\f\> s4 s4 s4 s4 s8 s8\! s4}>> 
  
  \myBreak
  
   \time 6/4
   
   r1  r4 \acciaccatura {gis8}g'!4\mf
   
   g,!2 r4 r4 g4.:32^\markup{flt.} r8
   g2.\f a2.\mf
   r2. f2.\p^\rallentando
   (f4) cis2.\f  r2
   
   \myBreak
   \tempo 4 = 40

   R1*6/4
   R1*6/4
   R1*6/4
   R1*6/4
   
   \myBreak

   \time 4/4
   r4 <<{cis2 (cis8)}{\alniente s8\< s8 \alniente s8\mf\> s8 s8\!}>> r8
   R1*4/4
   <<{R1*4/4} {s4 s4 s4%^\bins 
               s4}>>
   %fine A
  
  \tempo 4=70
  <<{f''2^\errearrow^\markup{flt.} (f2^\errearrow:32) (f2^\markup{no flt.})}{s4\pp\< s4 s4 s4 s4 s4\f}>> r2%^\manill
  
    \myBreak

   r1
   r1
   r1
   
     \myBreak
   \tempo 4=60

     a,,4.^>\f
     r8 c,16\mf r8. r4
     <f'\ff \tram \parenthesize f,>4\f^\tongueram r4 r4 <f\tram \parenthesize f,>^\tongueram
     (<f \tram \parenthesize f,>8) r8
     \fancy-gliss
        #'(
         (0 0 0.6 -0.5 1 0.5)
         
         )
     f2.\mf\<\glissando^\chiusap%^%\markup{\column{
                     %\line{Cresce girando l'imboccatura}
        %             \line{da tutta chiusa a tutta aperta}
                           
     %}}
     g4\f r2.
     
     
     <<{e'1}{s4\p\<
              s4 s4 s4\f}>>%^\markup{Cresce normalmente} 
     
          \myBreak

     \time 6/4
     
     g,2.\p <<{gis2.\mf}{a'2.}{dis2.^\primomultifonico}%^\blu
     >>
  
     r1.
     
     \myBreak
     
     \tempo 4=90
     b,2.\mf\<_\( c2.
     (c4\f)\) r1 r4
     

     dis,,1.\f
     
      \myBreak    

     r1 bes'2
     (bes2) r8 dis,8^\( ( (dis4) (dis8) f8 (f4)
     (f8)\) e8 dis8 c8 \breathe e2. fis4
     (fis1) \headPizz c16\f^\pizzicato\staccato   \headPizz c16\staccato  <<{f''16^\ordinario c16 cis8 d8
     (d2)} {\alniente s16\f\> s16 s8 s8  s4 s4\!}>>  <<{b,!1} {\alniente s4\f\>^\apchius s4 s4 s8 s8\!}>> \breathe
     

     <<{e,1.:32^\markup{flt.}} {\alniente s4\mf\> s4 s4 s4 s4 s4\!}>>
               \myBreak

     \time 7/4
     
     \override NoteHead.style = #'harmonic 
          %\break

     %\set glissandoMap = #'((0 . 1) (1 . 0))
     r16 c'2^\jw^\markup\rotate #-65 \curvedArrow #-0.6 #4 #0 %<<{c'2}{\hide  e''8\glissando s8 s8  \once \hideNotes f,8}>>
     
     r4 r4.. d4.^\markup\rotate #-75 \curvedArrow #0.1 #4.25 #0  r16 dis16^\markup\rotate #-55 \curvedArrow #5 #4.15 #0  
     _(dis2) \revert NoteHead.style cis8^\ordinario cis8 (cis4) (cis2) r8 c16\sfz^>\staccato r16
     
               \myBreak

    r16  \pitchedTrill c2 \startTrillSpan^\markup{irregolare}  cis r16\stopTrillSpan   <<{ cis8:32^\markup{flt.} (cis2.:32)}{\alniente s8\>\f s4 s4 s8\!}>> r8 dis16 r16 %alternativa a quello sotto
     %r16  \pitchedTrill c16\startTrillSpan^\markup{irregolare}  cis (c8)(c4)(c16) r16\stopTrillSpan   <<{ cis8:32^\markup{flt.} (cis2.:32)}{\alniente s8\>\f s4 s4 s8\!}>> r8 dis16 r16 %con warning
     cis2~ \f\trill <<{cis2^\secondomultifonico}%^\viola
                        {fis2}>> r2.
     
               

                                         \override NoteHead.style = #'harmonic 
                                         r8 fis2..^\jw^\markup\rotate #-65 \curvedArrow #3 #4.35 #0
  r4 \acciaccatura{g16} c,2^\markup\rotate #-65 \curvedArrow #0.2 #4 #0 
                                          \revert NoteHead.style
                                          
                                                    \myBreak

                                          \alniente c1^\ordinario\< (c2.\fff)
                                          r16 \alniente \acciaccatura {d32} cis16\<_\( (cis8) (c4) cis16 c8.\f\) \breathe r8 \acciaccatura {e16} d8 (d4) r16 c16\p (c16) r16 cis8\mp r8 
                                                   
                                                   \myBreak

                                          \time 5/4
                                          d'2.\mf r8 {fis8^\markup{flt.}}:64\f (fis16:64) r16 fis16\sfz r16
                                         R1*5/4
                                         \time 5/8
                                        
                                         \headPizz d,,16\ff^\pizzicato \headPizz d16 r16 fis16\mf^\ordinario r8  r16 \alniente cis16\<^\errearrow^\markup{flt.} (cis8 )
                                         (cis4:8 cis4:32\ff) r8
                                         
                                          \myBreak
                                          
                                         r16 <<{cis16\f\>^\ordinario^\( (c8) cis4 (cis16) } {s8 s8 s8 s8\mf}>> \breathe \alniente cis16\ff_\markup{subito \dynamic pp}
                                         (cis4\!\))  g''4.\pp\<
                                         (g8\mf)  r4 g,\mf\<
                                         (c,8\f) <<{cis2}{\alniente s8\< s8 s8 s8\ff}>> \breathe
                                         
                                                   \myBreak

                                         \tempo 4=30
                                         \time 7/4
                                         r8 \pitchedTrill d2\mp\<\startTrillSpan^\markup{irregolare} dis dis8 (dis4) (dis8)\mf\stopTrillSpan r8 <<{fis2:32}{\alniente s8\< s8 s8 s8\f}>>
                                         
                                         ais8\pp r8 ais2\mf r16 ais16\sfz r16 e'16^\errearrow^\markup{flt.}_\( (e4) (e8:16) (e16:32) (e4:32) f16^\markup{no flt.}\)
                                         r16 <<{\acciaccatura {dis'32} \alniente e,16\< (e8)(e4)(e16)}{s8 s8 s8 s8\mp}>> r16 <<{\acciaccatura {dis'32} \alniente f,16\< (f16) (f4) (f8)}{s8 s8 s8 s8\ff}>> r16 <<{\acciaccatura {cis'32} \alniente g,16\< (cis4)(cis8)(cis16)}{s8 s8 s8 s8\mf}>> cis'16\sfz 
                                         r16 g16\pp (g8)(g4)( g4) (g16) r16 e8\mf (e4) f2\f\> (f4\p) (f8) f8\mf\staccato (f4)  <<{\acciaccatura {cis'32\f} bes1}{\alniente s4\< s4 s4 s4\f}>>
                                          \override Staff.Clef.full-size-change = ##t
                                         %\clef treble
                                         r16 \acciaccatura{bes32} cis16 (cis8) (cis4) (cis16) r16 <d\ff \tram \parenthesize d,>8\f^\tongueram r8 <dis\ff \tram \parenthesize dis,>8\f \acciaccatura{e} f4 r16 \acciaccatura{e32} f16 (f8) (f16) r16 \acciaccatura{f32} fis8
                                         r8 \acciaccatura{cis16} g'4  <<{\alniente gis,,8\<  (gis4) (gis8\mf)} {s4 s4 s4\!}>> r16 \acciaccatura{gis32} d'16 (d2)
                                         <<{b'2^\terzomultifonico}%^\verde
                                                     {bes,2\pp^\markup{graduale}}>> dis'16\mf
                                         r16 r16 <<{b16\pp\<^\terzomultifonico%^\verde 
                                                                              (b4)(b8) (b16)}{bes,16 (bes4)(bes8)(bes16)}>> dis'16\mp (dis4) (dis16) \acciaccatura{fis16} f8.
                                         r16 f16 (f16) r16 r16  dis16 (dis8) (dis4) (dis16) r16 \acciaccatura{g32} gis8 (gis4) (gis8)  \breathe r16 gis16 (gis4)
                                         <<{e2\pp\<^\quartomultifonico%^\multifonico_la
                                                                 }{c2}{a'2}>> r16\! 
                                         <<{ais16\mp (ais8) (ais4) (ais8)}{s4  \alniente s4\> s4 s4 s16\!}>> r8  
                                         r2 <<{b1\ppp (b4)(b1.)(b4)} {s4\< s4 s4 s4 s4 s4  s4 s4 s4 s4 s4 s4\fff} >>
                                         
                                         \bar "|."
}