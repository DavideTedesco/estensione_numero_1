frfl = \markup{Freeze flauto \circle "1"}
triangoloup = 
\markup \stencil 
#(make-connected-path-stencil
  '((1 2)  ;; path coordinates
    (2 0)
    (0 0))
  0.3 ;; line thickness
  1  ;; X-axis scaling factor
  1  ;; Y-axis scaling factor
  #f  ;; auto-connect path to origin point? (0 0)
  #t)  % filled path?

tristencil =
#(ly:make-stencil (list 'embedded-ps
    "gsave
      currentpoint translate
      newpath
      0 -0.2 moveto
      1 1.8 lineto
      2 -0.2 lineto
      0 -0.2 lineto
      closepath
      fill
      grestore" )
    (cons 0 1.3125)
    (cons -.75 3))

tristencildown =
#(ly:make-stencil (list 'embedded-ps
    "gsave
      currentpoint translate
      newpath
      0.2 0.2 moveto
      0.2 0.2 lineto
      2.2 0.2 lineto
      1.2 -1.8 lineto
      closepath
      fill
      grestore" )
    (cons 0 1.3125)
    (cons -.75 3))

triangolodown = 
\markup \stencil 
#(make-connected-path-stencil
  '((1 2)  ;; path coordinates
    (2 0)
    (0 0))
  0.15  ;; line thickness
  1  ;; X-axis scaling factor
  -1  ;; Y-axis scaling factor
  #f  ;; auto-connect path to origin point? (0 0)
  #t)  % filled path?

%======TRIANGLENOTE

triangolouponce = {
  \once \override Dots.dot-count = #0 %https://lilypond.org/doc/v2.21/Documentation/notation/writing-rhythms
   \once            \hide Stem
\once\override NoteHead #'no-ledgers = ##t
  \once\override Accidental #'stencil = ##f
  \once\override NoteHead.stencil = \tristencil   } 

triangolodownonce = {
  \once \override Dots.dot-count = #0 %https://lilypond.org/doc/v2.21/Documentation/notation/writing-rhythms
\once \hide Stem
\once\override NoteHead #'no-ledgers = ##t
  \once\override Accidental #'stencil = ##f
  \once\override NoteHead.stencil =\tristencildown   } 




%----------------GLISSANDO NOTE NON ADIACENTI
%https://lsr.di.unimi.it/LSR/Snippet?id=662

guide = #(define-music-function (shift pad) (pair? number?) #{
 	\once \override TextSpanner.dash-period = #1.5
    \once \override TextSpanner.bound-details.left.padding = #-3
    \once \override TextSpanner.bound-details.right.padding = #1
    \once \override TextSpanner.extra-offset = #shift 
    \override TextSpanner.bound-details.right.Y = #pad
    \once \override TextSpanner.bound-details.right.text = #'()
        \once \override TextSpanner.thickness = #4
#})

gliss = #(define-music-function (shift pad) (pair? number?) #{
 	\once \override TextSpanner.style = #'line
    \once \override TextSpanner.bound-details.left.padding = #'-3
    
    \once \override TextSpanner.extra-offset = #shift 
    \override TextSpanner.bound-details.right.Y = #pad
    \once \override TextSpanner.bound-details.right.text = #'()
    \once \override TextSpanner.thickness = #4
#})
% Short command for start and stop  TextSpan
#(define Stsp startTextSpan )
#(define Stpsp stopTextSpan )

alniente = \once \override Hairpin.circled-tip = ##t

%------------------------------------------
fixed_media = {
    \override Staff.StaffSymbol.line-count = #1 %a che serve? 
     \hide Clef
       \numericTimeSignature
      %spazi lasciati per esteso e non raggruppati per poter inserire fixed_media all'occorrenza in ogni posizione
     s1^\markup{\huge \circle{1}}^\markup{\column{\box{\line{Fixed Media}}}} s2.
     s1 s2.
     s2^\markup{\huge \circle{2}} s2 s2.
     
      \myBreak
     
     s1
     s1
     s1
     s1
     
      \myBreak
      
     s1
     s1
     s1
     s1
     
      \myBreak
      
     s1
     s1
     s1
     s1
     s1
     s1^\markup{\huge \circle{3}}
     
      \myBreak
      
     s1.
     s1.
     s1.
     s2. s2.^\markup{\huge \circle{4}}
     s1.
     
  \myBreak
      
     s1.
     s1.
     s1.     
     s1.
     
      \myBreak
      
     s1^\markup{\huge \circle{5}}
     s1
     s1
     s1
     s1
     
      \myBreak
      
     s1
     s1
     s1
     
      \myBreak
      
     s1
     s1
     s1
     s1
     s1
     
      \myBreak
      
     s1.
     s1.^\markup{\huge \circle{6}}
     s1.
     
      \myBreak
      
     s1.
     s1.
     
      \myBreak
      
     s1.
     s1.
     s1.
     
      \myBreak
      
     s1.
     s1.
     s1.
     
     \myBreak
     
     s1. s4
     s1. s4
     
     \myBreak
     
     s1. s4
     s1. s4
     s1. s4
     
     \myBreak
     
     s1. s4
     s1. s4
     
     \myBreak
     
     s1 s4^\markup{\huge \circle{7}}
     s1 s4
     
     \myBreak
     
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     
     \myBreak
     
     s1 s2.
     s1 s2.
     
     \myBreak
     
     s1 s2.
     s1 s2.
     
     \myBreak
     
     s1 s2.
     s4 s4 s4 s8 s8^\markup{\huge \circle{8}} s2.
     
     \myBreak
     
     s1 s2.
     s1 s2.     
     
     \myBreak
     
     s1 s2.
     s1 s2.
          
     \myBreak
     
     s1 s2.
     s1 s2.
} 


\include "lmos.ly"

%=========DELAY
 
delay= {
    \override Staff.BarLine.bar-extent = #'(0 . 0) %https://lsr.di.unimi.it/LSR/Snippet?id=1072
   \override Staff.StaffSymbol.line-count = #1
    % \override Staff.StaffSymbol.line-positions = #'(0 1.8)

    %\override StaffSymbol.thickness = #(magstep 10)
    \hide Clef
       \numericTimeSignature
     \hideNotes
         
       s1^\markup{\box {\column {{\line{Delay RM}
                                                                     }}}} s2.
     s1 s2.
     s1 s2.
      \myBreak
     
     s1
      s1
      
      s4 s8 %\once \override Staff.BarLine #'extra-offset = #'(3.1 . 0)  \bar "!" 
      \alniente  s8^\markup{\box {\column {{\line{Delay RM}  
                                                                     \line{22 ms}}}}}\<   s2  
      \alniente s4\ff\> s4\! s2
     
      \myBreak
      
     s1
     s1
     s1
     s1
     
      \myBreak
      
     s1
     s1
     s1
     s1
     s4. \alniente s8^\markup{\box {\column {{\line{Delay RM} \line{34 ms}}}}}\< s2
      s8 s8 s4\ff \alniente  s8\> s8 s4\!
     
      \myBreak
      
     s1.
     s1.
     s1.
     s1.
     s1.
     
  \myBreak
      
     s1.
     s1.
     s1.     
     s1.
     
      \myBreak
      
     s1
     s1
     s1
     \alniente s2^\markup{\box {\column {{\line{Delay RM}
                                                                     \line{38 ms}}}}}\< s2\f
     \alniente  s4\> s4\! s2
     
      \myBreak
      
     s1
     s1
     s1
     
      \myBreak
      
     s1
     s1
     s1
     s1
     \alniente s4^\markup{\box {\column {{\line{Delay RM}
                                                                     \line{30 ms}}}}}\< s2.\f
     
      \myBreak
      
     s1 \alniente s4\> s4\!
     s1.
     s1.
     
      \myBreak
      
     s1.
     s1.
     
      \myBreak
      
     s1.
     s1.
     s1.
     
      \myBreak
      
     s1.
     s1.
     s1.
     
     \myBreak
     
     s1. s4
     s1. s4
     
     \myBreak
     
     \alniente s1.^\markup{\box {\column {{\line{Delay RM}
                                                                     \line{10 ms}}}}}\<\alniente s8\f \> s8\!
     s1. s4
     s1. s4
     
     \myBreak
     
     s1. s4
     s1. s4
     
     \myBreak
     
     s1 s4
     s1 s4
     
     \myBreak
     
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     s2 s8
     
     \myBreak
     
     \alniente s1^\markup{\box {\column {{\line{Delay RM}
                                                                     \line{15 ms}}}}}\< s4\f s4 \alniente s8 \> s8\!
     s1 s2.
     
     \myBreak
     
     \alniente s1^\markup{\box {\column {{\line{Delay RM}
                                                                     \line{22 ms}}}}}\< s2.
     s1 s4\f s4 \alniente s8 \> s8\!
     
     \myBreak
     
     s1 s2.
     s4 s4 s4 s8 s8 s2.
     
     \myBreak
     
     s1 s2.
     \alniente s1^\markup{\box {\column {{\line{Delay RM}
                                                                     \line{30 ms}}}}}\< s4\f s4 \alniente s8 \> s8\!    
     
     \myBreak
     
     \alniente s1^\markup{\box {\column {{\line{Delay RM}
                                                                     \line{45 ms}}}}}\< s4\f s4 \alniente s8 \> s8  
     s4 s4 s2\! s2.
          
     \myBreak
     
     s2\alniente s4^\markup{\box {\column {{\line{Delay RM}
                                                                     \line{100 ms}}}}}\< s4 s2.
     s4\f \alniente s4\> s4 s4\! s2.
}
%=========
secondavoce = %\relative c''
{ 
  
 
    
<<
    \new Staff = "fixed_media" \fixed_media
    \new Staff = "lmo_uno" \lmo_uno
    \new Staff = "whiteNoiseLMO" \whiteNoiseLMO
    %\new Staff = "lmo_tre" \lmo_tre
    %\new Staff = "lmo_quattro" \lmo_quattro
    \new Staff = "delay" \delay
  >>
}   
   
