%da compilare con LilyPond-book
\documentclass{book}
\usepackage{hyperref}
\usepackage {pdfpages}
\usepackage[margin=.7in]{geometry}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{imfellEnglish}
\usepackage[T1]{fontenc}
\usepackage{afterpage}
\usepackage{tipa}
\usepackage{fancyhdr} 
%\usepackage{fancy}
\usepackage[T2A,T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{pstricks}
\usepackage{xcolor}
\usepackage{enumitem}
\usepackage{tikz}
    \usetikzlibrary{cd}
    \usetikzlibrary{shapes.misc}
    
\fancyhf{}
\cfoot{\thepage}
\pagestyle{fancy}    
\usetikzlibrary{calc}
\pagenumbering{roman}

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

\newcommand\blankpage{%
    \null
    \thispagestyle{empty}%
    \addtocounter{page}{-1}%
    \newpage}

\begin{document}

\title {\bf{Studio sul Corpo d'Ombra \# 1}
	\\ %sottotitolo
	}
\author{Davide Tedesco}
\date{Roma-Siena \\ 2023}

\maketitle
\newpage 
\begin{center}
\fbox{per Flauto Basso e Live Electronics}%parte per Flauto Basso
\\
\vfill
Draft per solo Flauto Basso (senza elettronica) del 17 ottobre 2023%da cambiare
\vfill
%\ % The empty page
\addtocounter{page}{-2}%
\renewcommand{\headrulewidth}{0pt}

\newpage
\renewcommand{\headrulewidth}{1.5pt}

\end{center}

%===================================
%============INTRODUZIONE==========
%===================================
%Introduzione
\section *{Introduzione}

\subsection *{Il brano}
Il brano fa parte di una raccolta di studi per Flauto Basso, Voce Maschile ed Live Electronics basata sul testo \textit{\hyperref[sec:cdo]{Corpi d'Ombra}}, rielaborazione della poesia del poeta simbolista Arthur Rimbaud \textit{Voyelles}, quest'ultimo vede come elementi poetici   fondanti i suoni, le allitterazioni e gli ossimori descritti per mezzo di una strutturazione basata sulle vocali.
La rielaborazione pone centralmente le figure del corpo e della sua ombra in variegate prospettive.
Quello qui riportato è il primo degli studi, incentrato sul rapporto tra Flauto Basso ed elementi di tape, di sintesi e di elaborazione del suono in tempo reale.
In esso si evincono alcuni tratti distintivi che legano lo strumento e l'elettronica: il rumore, il soffio ed il respiro. 
 


%===========ORGANIZZAZIONE DELLA PARTITURA=============

\subsection *{Organizzazione della partitura}
Studio sul Corpo d'Ombra \# 1 è uno studio per flauto basso e Live Electronics. 
Nel brano vengono utilizzate tecniche esplicate all'interno della \hyperref[sec:simus]{legenda dei simboli musicali}.
Per il Live Electronics sono indicate le \textit{cues} utili per l'esecuzione del brano e le azioni da compiere per tutta la durata dell'esecuzione.
%da finire

%===========ORGANICO=============

\subsection *{Organico}
\begin{itemize}
\item{Flauto Basso}
%\item{Voce}
\item{Live Electronics}
\end{itemize}
Ad essi si possono aggiungere tutte quelle figure che possono coadiuvare la messa in scena del brano e la regia del suono.

%===========SISTEMA DI DIFFUSIONE=============

\subsection *{Il sistema di diffusione}
Il sistema di diffusione utilizzato è composto da uno S.T.One\footnote{o altra tipologia di amplificazione...} ed un subwoofer.
La microfonazione richiesta... %da definire 

%=============DISPOSIZIONE DEGLI ESECUTORI======
\newpage
\section *{Disposizione sulla scena}
\subsection *{Disposizione dall'alto del palco}
\begin{center}
\begin{tikzpicture}

		\draw[line width=0.5mm]
		    (0,2) -- (14,2)  node[right] {proscenio};
				   
		\draw[line width=0.5mm]
		    (0,12) -- (14,12)  node[right] {fondo};

		\node [diamond,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,8){S.T.One}; 
		
		\node [rectangle,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,10){Subwoofer}; 

		\node[circle,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,4){Flauto}; 
\end{tikzpicture}
\end{center}

\subsection *{Disposizione frontale del palco}
\begin{center}
\begin{tikzpicture}

		\draw[line width=0.5mm]
		    (0,2) -- (14,2)  node[right] {pavimento};
				   
		\draw[line width=0.5mm]
		    (0,12) -- (14,12)  node[right] {soffitto};

		\node [diamond,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,8){S.T.One}; 
	
\node [rectangle,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,3){Subwoofer}; 
		\node[circle,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,6){Flauto}; 
\end{tikzpicture}
\end{center}

\newpage

%===========TESTO=============

\newpage
\section *{Il testo}\label{sec:cdo}
%\begin{center}
%Davide Tedesco
\subsection *{Corpi d'Ombra}
%\end{center}
\vspace{1cm}
%\thispagestyle{empty}
%\setcounter{page}{23} %da cambiare
\hspace{0.5cm}Ombra, nera

anima lucente

Orma, nera, fragranza vissuta, ombra sbiadita, esseri vibranti\newline



Candida luce

che fa specchiar l’anima saggia

nel ghiaccio, flora sempreverde che pur trema\newline



Sangue fisico

Bocca gaia 

nella collera o nelle ebrezze penitenti\newline \\ \\



Tondi e smeraldei luoghi di pace

fauna vuota di uomini incauti

ricca di quiete che dona

riflessione ed alchimia agli studiosi\newline \\ \\ \\



Ora che l’ombra

rinnega il suo corpo

Come corpi, come cor,

come corpo

come ombra, come corpo d’ombra

Stridori e silenzi di Universo ultraterreno ed umano

facciano congiungere l’Omega per mezzo del viola flusso

al suo sguardo\newline \\ \\ \\



Corpo vuoto

unione di sangue

legame di anima ed ombra

Corpo sperso nei golfi d’ombra

Viaggio iridato custode del passaggio del tempo

%===================================
%==============LEGENDA=============
%===================================
%Legenda dei simboli
\newpage
\section *{Legenda dei simboli}


%===LEGENDA DEI SIMBOLI MUSICALI=====
\subsection *{Simboli musicali}\label{sec:simus}

\subsubsection *{Flauto}

\begin{itemize}
\item{
Jet Whistle\newline\newline
La freccia discendente indica la durata e la scansione degli armonici, infatti controllando l'emissione del fiato si possono tentare di "arpeggiare" gli armonici ponendo il focus maggiormente su alcuni di essi; in tal caso il punto più elevato della curva della freccia si prefigura come l'armonico più elevato che si può raggiungere e le altre altezze come durata seguono di conseguenza.\newline
\hspace*{0.99cm}
\includegraphics[width=20mm,height=20mm,keepaspectratio]{tr.png}%da sostituire con lilypond



\iffalse%++++++++++++++++++++++++++++++++++DISABILITATO

\begin{lilypond}[fragment,relative=2,quote,staffsize=16]

\version "2.22.2"


 \new Staff \with{ 
 \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
 
} {
 \override NoteHead.style = #'harmonic 
c2^\markup\rotate #-75 \curvedArrow #0.1 #4.25 #0
}
\end{lilypond}
\fi%+++++++++++++++++++++++++++++++++++++++++++++++++

}

\item{
Tongue Ram\newline\newline
In cui la nota superiore indica la posizione da prendere e quella inferiore fra parentesi indica la risultante.\newline
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {


<f   \tweak style #'triangle  \parenthesize  f,>8^\markup{t.r.}


}
\end{lilypond}
}

\item{
Pizzicato\newline\newline
Come riportato in \textsc{Pierre Ives Artaud, Gérard Geay} - \textit{Present Day Flutes} che citiamo di seguito:
\textit{To produce a pizzicato, tongue the palette (TE) without blowing any air...One can use the three tonguing patterns of the flautists (single: TE, double: TE KE TE KE, or triple: TE KE TE TE KE TE an thus attain a very rapid movement.\newline As the lungs retain the air, it is necessary to breathe very frequently (no more than a few seconds without breathing).}

	
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
\once \override NoteHead.stencil = #ly:text-interface::print
\once \override NoteHead.text = #(markup #:musicglyph "scripts.sforzato") 
\once \override NoteHead.font-size = #0 d,16^\markup{ \column{ \line{pizz.}}}


}
\end{lilypond}
}

\item{
Apertura e chiusura dell'imboccatura\newline
\newline
Aprire o chiudere l'imboccatura con le labbra (il risultato è un lieve crescendo o decrescendo dell'altezza).\newline
\begin{itemize}
\item{
Da aperto a chiuso
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
b,1^\markup {

               
                 
                \override #'(thickness . 2)
                  \translate#'(1 . 0)\overlay {
      \draw-circle #0.5 #0.2 ##f
      
         \translate #'(1 . 0)\draw-line #'(14 . 0)
       
          \translate #'(16 . 0)\arrow-head #X #RIGHT ##t
      \translate #'(17 . 0)\draw-circle #0.5 #0.2 ##t

                  }

  }
}
\end{lilypond}

}
\item{
Da chiuso ad aperto
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
b,1^\markup {

               
                 
                \override #'(thickness . 2)
                  \translate#'(1 . 0)\overlay {
      \draw-circle #0.5 #0.2 ##t
      
         \translate #'(1 . 0)\draw-line #'(12 . 0)
       
          \translate #'(14 . 0)\arrow-head #X #RIGHT ##t
      \translate #'(15 . 0)\draw-circle #0.5 #0.2 ##f

                  }

  }
  }
\end{lilypond}

}

\end{itemize}
}

\item{
Flatterzunge con entrata o uscita progressiva della "R"
\newline\newline
Flatterzunge facendo entrare progressivamente la "R", usato alle volte in maniera inversa ovvero facendo uscire progressivamente la "R".
\newline
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
fis2^\markup {

               \right-align
                 \hspace #2
                 
                \override #'(thickness . 2)
                  \overlay {

        \draw-line #'(7 . 0)

          \translate #'(7 . 0)\arrow-head #X #RIGHT ##t

                  }

  }^\markup{flt.} (fis:32)
}
\end{lilypond}	
}

\newpage
\item{
Trillo irregolare\newline\newline
Trillo utilizzando l'altezza tra parentesi ma con durate non regolari.
\newline
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
\pitchedTrill d2\mp\<\startTrillSpan^\markup{irregolare} dis \hideNotes d2
}
\end{lilypond}	
}



\item{
Multifonici
\newline\newline
Essi sono riportati in partitura per mezzo di un quadrato con all'interno inscritto il numero dello specifico multifonico, ad esempio per il primo \fbox{1}.\newline

\begin{enumerate}[label=\fbox{\arabic*}]

%uno
\item{
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{ \remove Time_signature_engraver      \hide Staff.BarLine
} {
\clef "treble_8"
<gis a' dis>2.^\markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one two three four five six))
           (lh . (b gis))
           (rh . ())
           )}
    
    
}

\end{lilypond}	
}

%due
\item{
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{ \remove Time_signature_engraver      \hide Staff.BarLine
} {
\clef "treble_8"

<cis, fis>2^\markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one two three ))
           (lh . (b ))
           (rh . (cis dis))
           )
    }
    
}

\end{lilypond}	
}

%tre
\item{
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{ \remove Time_signature_engraver      \hide Staff.BarLine
} {
\clef "treble_8"

<b, bes,>2^\markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one  three four five ))
           (lh . (bes ))
           (rh . (ees))
           )
    }    
}

\end{lilypond}	
}

%quattro
\item{
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{ \remove Time_signature_engraver      \hide Staff.BarLine
} {

<e c a'>2^\markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (two five ))
           (lh . (b))
           (rh . (ees))
           )
    }
    
}

\end{lilypond}	
}


\end{enumerate}
}
\end{itemize}

\newpage

%===================================
%==============PARTITURA===========
%===================================

%Include della partitura completa

%macOS
\includepdf [pages=-]{/Users/davide/gitlab/DavideTedesco/estensione_numero_1/docs/Davide_Tedesco-Studio_sul_Corpo_dOmbra_Numero_1-lilypond/Davide_Tedesco-Studio_sul_Corpo_dOmbra_Numero_1_flauto.pdf}
%Linux
%\includepdf [pages=-]{/home/davide/gitlab/DavideTedesco/estensione_numero_1/references/Davide_Tedesco-Studio_sul_Corpo_dOmbra_Numero_1-lilypond/Davide_Tedesco-Studio_sul_Corpo_dOmbra_Numero_1_flauto.pdf}

\end{document}