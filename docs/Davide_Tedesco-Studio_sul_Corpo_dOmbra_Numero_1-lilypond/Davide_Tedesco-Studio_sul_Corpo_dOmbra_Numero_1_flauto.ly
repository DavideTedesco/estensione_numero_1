%[1]   VERSIONE ==============
\version "2.20.0"

%[2]  BLOCCO PAPER==================================
%\include "template/papera4P_with_page_number.ly"
\include "_setup.ly"

%[3]  BLOCCO HEADER==================================
date = #(strftime "Versione del %d-%m-%Y" (localtime (current-time)))

 \header { 
                 title = "Studio sul Corpo d'Ombra # 1"
                 composer = "Davide Tedesco"	
                 subtitle = "per Flauto Basso e Live Electronics"
                 %subsubtitle =   \date
                 tagline = ""
                 %dedication = "ad Elena D'Alò"
                
 } 

%[4]  BLOCCO NOTAZIONE==================================

\include "personalizzazioni.ly"
\include "arrows.ly"

\include "righi/flautobasso.ly"
\include "righi/elettronica.ly"


%\include "voci/tempo.ly"


%[5]  BLOCCO IMPOSTAZIONE RIGHI==========================
\include "template/FlautoBasso_Live_Electronics_template.ly"


%[6]  BLOCCO SCORE==================================
       \score {

%[6a]  parti da includere========================


       %\rigozero
       \new ChoirStaff<<
        \primorigo
       >>
      
	


%[6b] PERSONALIZZAZIONE GRAFICA==========
 \layout {
            %indent = -1
    \context {
      \Voice
            \consists "Duration_line_engraver" %https://lilypond.org/doc/v2.23/Documentation/notation/graphical-notation.it.html
            %\override NoteHead.duration-log = 1 %descrive la lungheza di ogni linea
              \override DurationLine.bound-details.right.padding = -2 %sovrappone la linea a quella successiva
%=============https://lsr.di.unimi.it/LSR/Snippet?id=232   
   % The self-alignment-X property tells LilyPond to position the marks on
% their right edges.
      %\override DynamicText.self-alignment-X = #1
% The X-offset property normally is set to ly:self-alignment-interface::x-aligned-on-self,
% which is a function that says what the X-offset needs to be in order to align the mark
% on its right edge. Here we tell it instead to use a function that indicates an offset 1.2
% staff spaces to the left of right-alignment.  The 1.2 can be adjusted as needed to give
% different offsets.
      \override DynamicText.Y-offset =
        #(lambda (grob)
           (- (ly:self-alignment-interface::y-aligned-on-self grob)
           1.2))
      \override DynamicLineSpanner.direction = #DOWN
    
    }
    \context {
            %\Staff \RemoveEmptyStaves
      %\override VerticalAxisGroup.remove-first = ##t
      
      \Score
       \accidentalStyle default
      \override DynamicText.direction = #UP
      %per spostare le indicazioni dinamiche sopra la partitura
      \override DynamicLineSpanner.direction = #UP
      \override Flag.stencil = #modern-straight-flag
      
        \override Beam #'damping = #4 % ------------ INCLINAZIONE TRAVATURA
        \override Stem #'length-fraction = #'1.5
        \override NoteHead #'font-size = #-2
        \override DynamicLineSpanner.staff-padding = #5
      
      \override Glissando.thickness = #20/3
      % \hide SpanBar 
      %++++++++tweaks for propotional notation
      proportionalNotationDuration = #(ly:make-moment 1/8)
      \override SpacingSpanner.uniform-stretching = ##t
      \override Score.SpacingSpanner.strict-note-spacing = ##f

      %\override PaperColumn.used = ##t
      %++++++++tweaks for hiding bars, bar numbers and time
      %\hide Staff.BarLine
      %\hide TimeSignature
      %\omit BarNumber    
            \override TimeSignature.font-size = #6

     

    }
    %\context {
     % \Voice
     % \override HorizontalBracket.direction = #UP
      %\override HorizontalBracket.bracket-flare = #'(0 . 0)
      %\override HorizontalBracket.edge-height  = #'(1 . 1)
      %\override HorizontalBracket.shorten-pair =#'(0 . -2.5)
      %\consists "Horizontal_bracket_engraver"
      

    %}
  }
%[6c] OUTPUT MIDI===================
%\midi {}
} 

%[7] OPTIONS FOR EXPORT===================
%disable hyperlinks
%\pointAndClickOff