primorigo = \new Staff \with{
         
             instrumentName = "Flauto Basso"
        shortInstrumentName = "Fl. B."
                midiInstrument = "flute"
\consists "Page_turn_engraver" 

        
    }    {
      \primavoce}
    
secondorigo = \new PianoStaff  \with{
        instrumentName = "Live Electronics"
        shortInstrumentName = "L. E."
                midiInstrument = "choir aahs"
                   \hide Clef
                   
                         %\remove "Time_signature_engraver"
                \consists "Page_turn_engraver" 
                 
\new Voice{
        }
}{\secondavoce}

