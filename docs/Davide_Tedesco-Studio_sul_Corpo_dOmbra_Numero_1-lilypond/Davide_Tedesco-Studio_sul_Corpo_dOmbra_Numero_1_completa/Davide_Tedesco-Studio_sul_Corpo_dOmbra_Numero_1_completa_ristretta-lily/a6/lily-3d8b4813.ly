%% Generated by lilypond-book
%% Options: [fragment,indent=0\mm,line-width=513\pt,quote,relative=2,staffsize=16]
\include "lilypond-book-preamble.ly"


% ****************************************************************
% Start cut-&-pastable-section
% ****************************************************************

#(set-global-staff-size 16)

\paper {
  indent = 0\mm
  line-width = 513\pt
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
  line-width = 513\pt - 2.0 * 0.4\in
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
}

\layout {
  
}



\relative c''
{


% ****************************************************************
% ly snippet contents follows:
% ****************************************************************
\sourcefileline 573

	
\version "2.22.2"

}

 \new Staff \with{       \hide Staff.BarLine \hide Clef
} {
\override Staff.StaffSymbol.line-count = #1
   \override Staff.StaffSymbol.line-positions = #'(0)
  %\override StaffSymbol.thickness = #(magstep 10)
   \hide Clef
     \numericTimeSignature
     %\override TextScript.extra-offset = #'(0 . 11.1)
     %\override Glissando.thickness = #'2
     \hide Clef
       \numericTimeSignature
       \time 7/4
        \override TimeSignature.font-size = #6
             %spazi lasciati per esteso e non raggruppati per poter inserire fixed media all'occorrenza in ogni posizione

   \hideNotes %<<{
      \override System.top-system-spacing = #'2
      %s1. s4
      %s1 s2.
      %s1 s2 s4 
    
      s1.\pp^\markup{\box \column{\line{Live Multi Oscillator   \box 1 \box 2 \box 3  60 Hz }
                                                         %\line {\box 2 70 Hz}
                                                         %\line{\box 3  90 Hz}
                                                         %\line {\box 4 100 Hz}
                                                          }}%_\markup{ \box 1  60 Hz}
           s4
       
    
   
}




% ****************************************************************
% end ly snippet
% ****************************************************************
}
