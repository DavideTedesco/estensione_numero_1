%% Generated by lilypond-book
%% Options: [fragment,indent=0\mm,line-width=513\pt,quote,relative=2,staffsize=16]
\include "lilypond-book-preamble.ly"


% ****************************************************************
% Start cut-&-pastable-section
% ****************************************************************

#(set-global-staff-size 16)

\paper {
  indent = 0\mm
  line-width = 513\pt
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
  line-width = 513\pt - 2.0 * 0.4\in
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  line-width = #(- line-width (* mm  3.000000) (* mm 1))
}

\layout {
  
}



\relative c''
{


% ****************************************************************
% ly snippet contents follows:
% ****************************************************************
\sourcefileline 661

	
\version "2.22.2"

 \new Staff \with{       \hide Staff.BarLine \hide Clef
} {
\time 7/4
    \override Staff.StaffSymbol.line-count = #1
   \override Staff.StaffSymbol.line-positions = #'(0)
  %\override StaffSymbol.thickness = #(magstep 10)
  \hide Clef
     \numericTimeSignature
   \hideNotes
       \once \override Hairpin.circled-tip = ##t d1^\markup{\column \box{{\line{Delay 10 ms} }}}\< d2.
     d1 \once \override Hairpin.circled-tip = ##t  d2.\f\>
     s1\! s2.
}




% ****************************************************************
% end ly snippet
% ****************************************************************
}
