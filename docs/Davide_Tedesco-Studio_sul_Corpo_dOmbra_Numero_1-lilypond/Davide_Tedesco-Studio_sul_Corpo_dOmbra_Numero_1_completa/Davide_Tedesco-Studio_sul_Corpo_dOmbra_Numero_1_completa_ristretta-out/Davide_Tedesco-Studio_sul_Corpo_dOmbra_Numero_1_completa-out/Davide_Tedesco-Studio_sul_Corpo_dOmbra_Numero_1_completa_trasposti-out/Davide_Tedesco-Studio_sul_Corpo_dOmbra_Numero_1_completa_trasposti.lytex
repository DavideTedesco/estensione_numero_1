%da compilare con LilyPond-book
\documentclass{book}
\usepackage{hyperref}
\usepackage {pdfpages}
\usepackage[margin=.7in]{geometry}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{imfellEnglish}
\usepackage[T1]{fontenc}
\usepackage{afterpage}
\usepackage{tipa}
\usepackage{fancyhdr} 
%\usepackage{fancy}
\usepackage[T2A,T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{pstricks}
\usepackage{xcolor}
\usepackage{enumitem}
\usepackage{tikz}
    \usetikzlibrary{cd}
    \usetikzlibrary{shapes.misc}
\usepackage{verbatim}

\fancyhf{}
\cfoot{\thepage}
\pagestyle{fancy}    
\usetikzlibrary{calc}
\pagenumbering{roman}

\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

\newcommand\blankpage{%
    \null
    \thispagestyle{empty}%
    \addtocounter{page}{-1}%
    \newpage}

\begin{document}

\title {\bf{Studio sul Corpo d'Ombra \# 1}
	\\ %sottotitolo
	}
\author{Davide Tedesco}
\date{Roma-Siena \\ 2023}

\maketitle
\newpage 
\begin{center}
\fbox{per Flauto Basso e Live Electronics}
\\
\vfill
Versione del 21 ottobre 2023 per solo flauto con suoni trasposti.%da cambiare giornalmente ad ogni compilazione
\vfill
%\ % The empty page
\addtocounter{page}{-2}%
\renewcommand{\headrulewidth}{0pt}

\newpage
\renewcommand{\headrulewidth}{1.5pt}

\end{center}

%===================================
%============INTRODUZIONE==========
%===================================
%Introduzione
\section *{Introduzione}

\subsection *{Il brano}
Il brano fa parte di una raccolta di studi per Flauto Basso, Voce Maschile ed Live Electronics basata sul testo \textit{\hyperref[sec:cdo]{Corpi d'Ombra}} (riportato di seguito), rielaborazione della poesia del poeta simbolista Arthur Rimbaud \textit{Voyelles}, quest'ultimo vede come elementi poetici fondanti i suoni, le allitterazioni e gli ossimori descritti per mezzo di una strutturazione basata sulle vocali.
La rielaborazione pone centralmente le figure del corpo e della sua ombra in variegate prospettive e visioni.\newline
Quello qui riportato è il primo degli studi, incentrato sul rapporto tra Flauto Basso ed elementi di fixed media, di sintesi e di elaborazione del suono in tempo reale.
In esso si evincono alcuni tratti distintivi che legano lo strumento e l'elettronica: il rumore bianco, il soffio ed il respiro. 
 


%===========ORGANIZZAZIONE DELLA PARTITURA=============

\subsection *{Organizzazione della partitura}
Studio sul Corpo d'Ombra \# 1 è uno studio per flauto basso e Live Electronics. 
Nel brano vengono utilizzate tecniche di emissione sonora esplicate all'interno della \hyperref[sec:simus]{legenda dei simboli musicali}.
Per il Live Electronics sono indicate le \textit{cues} utili per l'esecuzione del \textit{fixed media} e le azioni da compiere per tutta la durata dell'esecuzione. In particolare sono presenti:
\begin{itemize}
\item{una particolare tipologia di oscillatore}
\item{fixed media}
\item{delay con Ring Modulation}
\end{itemize}

%===========ORGANICO=============

\subsection *{Organico}
\begin{itemize}
\item{Flauto Basso}
%\item{Voce}
\item{Live Electronics}
\end{itemize}
Ad essi si possono aggiungere tutte quelle figure che possono coadiuvare la messa in scena del brano e la regia del suono.

%===========SISTEMA DI DIFFUSIONE=============

\subsection *{Il sistema di diffusione}
Il sistema di diffusione utilizzato è composto da uno S.T.ONE\footnote{o altra tipologia di amplificazione da concordare con il compositore che permetta un'emissione superiore, inferiore e laterale del suono posteriormente all'esecutore} ed un subwoofer. Lo S.T.ONE o il sistema di diffusione deve essere disposto (come per i microfoni Soundfield) con i 4 canali come di seguito\footnote{le matrici di conversione da \textit{A format} a \textit{B format} seguono tale disposizione, si consiglia la lettura del seguente articolo \href{http://pcfarina.eng.unipr.it/Public/B-format/A2B-conversion/A2B.htm}{link} del Prof. Angelo Farina}:
\begin{itemize}
\item{1 - Left Front Up}
\item{2 - Right Front Down}
\item{3 - Right Back Up}
\item{4 - Left Back Down}
\end{itemize}
La microfonazione richiesta è la TETRAREC\footnote{o altra tipologia di microfonazione da concordare con il compositore che permetta una ripresa dello strumento in ogni sua direzione di emissione, si segua anche in tale caso la disposizione dei microfoni come quella del sistema di diffusione} disposta attorno all'esecutore ponendo come punto focale della stessa la bocca dell'esecutore.
\newpage

%=============DISPOSIZIONE DEGLI ESECUTORI======
\newpage
\section *{Disposizione sulla scena}
\subsection *{Disposizione dall'alto del palco}
\begin{center}
\begin{tikzpicture}

		\draw[line width=0.5mm]
		    (0,2) -- (14,2)  node[right] {proscenio};
				   
		\draw[line width=0.5mm]
		    (0,12) -- (14,12)  node[right] {fondo};

		\node [diamond,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,8){S.T.ONE}; 
		
		\node [rectangle,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,10){Subwoofer}; 

		\node[circle,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,4){Flauto}; 
\end{tikzpicture}
\end{center}

\subsection *{Disposizione frontale del palco}
\begin{center}
\begin{tikzpicture}

		\draw[line width=0.5mm]
		    (0,2) -- (14,2)  node[right] {pavimento};
				   
		\draw[line width=0.5mm]
		    (0,12) -- (14,12)  node[right] {soffitto};

		\node [diamond,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,8){S.T.ONE}; 
	
\node [rectangle,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,3){Subwoofer}; 
		\node[circle,draw=black, text=white, fill=black, minimum size=2cm] (c) at (7,6){Flauto}; 
\end{tikzpicture}
\end{center}

\newpage

%===========TESTO=============

\newpage
\section *{Il testo}\label{sec:cdo}
%\begin{center}

\subsection *{Corpi d'Ombra}
%\end{center}
\vspace{1cm}
%\thispagestyle{empty}
%\setcounter{page}{23} %da cambiare
\hspace{0.5cm}Ombra, nera

anima lucente

Orma, nera, fragranza vissuta, ombra sbiadita, esseri vibranti\newline



Candida luce

che fa specchiar l’anima saggia

nel ghiaccio, flora sempreverde che pur trema\newline



Sangue fisico

Bocca gaia 

nella collera o nelle ebrezze penitenti\newline \\ \\



Tondi e smeraldei luoghi di pace

fauna vuota di uomini incauti

ricca di quiete che dona

riflessione ed alchimia agli studiosi\newline \\ \\ \\



Ora che l’ombra

rinnega il suo corpo

Come corpi, come cor,

come corpo

come ombra, come corpo d’ombra

Stridori e silenzi di Universo ultraterreno ed umano

facciano congiungere l’Omega per mezzo del viola flusso

al suo sguardo\newline \\ \\ \\



Corpo vuoto

unione di sangue

legame di anima ed ombra

Corpo sperso nei golfi d’ombra

Viaggio iridato custode del passaggio del tempo\newline\newline
\vspace{1cm}
 \begin{flushright} Davide Tedesco \end{flushright}
 
%===================================
%==============LEGENDA=============
%===================================
%Legenda dei simboli
\newpage
%===LEGENDA DEI SIMBOLI MUSICALI=====

\section *{Legenda dei simboli musicali}\label{sec:simus}
%===FLAUTO==========================
\subsection *{Flauto}

\begin{itemize}
\item{
Jet Whistle\newline\newline
La freccia discendente indica la durata e la scansione degli armonici del Jet Whistle, infatti, controllando l'emissione del fiato si possono ``arpeggiare" gli armonici ponendo il focus maggiormente su alcuni di essi per un periodo temporale diverso gli uni dagli altri, la freccia indica quindi come gestire il fiato durante il singolo gesto musicale.\newline
\hspace*{0.99cm}
\includegraphics[width=20mm,height=20mm,keepaspectratio]{jw.png}%da sostituire con lilypond



\iffalse%++++++++++++++++++++++++++++++++++DISABILITATO

\begin{lilypond}[fragment,relative=2,quote,staffsize=16]

\version "2.22.2"


 \new Staff \with{ 
 \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
 
} {
 \override NoteHead.style = #'harmonic 
c2^\markup\rotate #-75 \curvedArrow #0.1 #4.25 #0
}
\end{lilypond}
\fi%+++++++++++++++++++++++++++++++++++++++++++++++++

}

\item{
Tongue Ram\newline\newline
In cui la nota superiore indica la posizione da prendere e quella inferiore fra parentesi indica la risultante.\newline
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {


<f   \tweak style #'triangle  \parenthesize  f,>8^\markup{t.r.}


}
\end{lilypond}
}

\item{
Pizzicato\newline\newline
Come riportato in \textsc{Pierre Ives Artaud, Gérard Geay} - \textit{Present Day Flutes} che citiamo di seguito:
\textit{To produce a pizzicato, tongue the palette (TE) without blowing any air...One can use the three tonguing patterns of the flautists (single: TE, double: TE KE TE KE, or triple: TE KE TE TE KE TE an thus attain a very rapid movement.\newline As the lungs retain the air, it is necessary to breathe very frequently (no more than a few seconds without breathing).}

	
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
\once \override NoteHead.stencil = #ly:text-interface::print
\once \override NoteHead.text = #(markup #:musicglyph "scripts.sforzato") 
\once \override NoteHead.font-size = #0 d,16^\markup{ \column{ \line{pizz.}}}


}
\end{lilypond}
}

\item{
Apertura e chiusura dell'imboccatura\newline
\newline
Aprire o chiudere l'imboccatura con le labbra (il risultato è un lieve crescendo o decrescendo dell'altezza).\newline
\begin{itemize}
\item{
Da aperto a chiuso
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
b,1^\markup {

               
                 
                \override #'(thickness . 2)
                  \translate#'(1 . 0)\overlay {
      \draw-circle #0.5 #0.2 ##f
      
         \translate #'(1 . 0)\draw-line #'(14 . 0)
       
          \translate #'(16 . 0)\arrow-head #X #RIGHT ##t
      \translate #'(17 . 0)\draw-circle #0.5 #0.2 ##t

                  }

  }
}
\end{lilypond}

}
\item{
Da chiuso ad aperto
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
b,1^\markup {

               
                 
                \override #'(thickness . 2)
                  \translate#'(1 . 0)\overlay {
      \draw-circle #0.5 #0.2 ##t
      
         \translate #'(1 . 0)\draw-line #'(12 . 0)
       
          \translate #'(14 . 0)\arrow-head #X #RIGHT ##t
      \translate #'(15 . 0)\draw-circle #0.5 #0.2 ##f

                  }

  }
  }
\end{lilypond}

}

\end{itemize}
}

\item{
Flatterzunge con entrata o uscita progressiva della ``R"
\newline\newline
Flatterzunge facendo entrare progressivamente la ``R", usato alle volte in maniera inversa ovvero facendo uscire progressivamente la ``R".
\newline
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
fis2^\markup {

               \right-align
                 \hspace #2
                 
                \override #'(thickness . 2)
                  \overlay {

        \draw-line #'(7 . 0)

          \translate #'(7 . 0)\arrow-head #X #RIGHT ##t

                  }

  }^\markup{flt.} (fis:32)
}
\end{lilypond}	
}

\newpage
\item{
Trillo irregolare\newline\newline
Trillo utilizzando l'altezza tra parentesi ma con durate non regolari.
\newline
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"
 \new Staff \with{ \remove Time_signature_engraver       \remove Clef_engraver \hide Staff.BarLine
} {
\pitchedTrill d2\mp\<\startTrillSpan^\markup{irregolare} dis \hideNotes d2
}
\end{lilypond}	
}



\item{
Multifonici
\newline\newline
Essi sono riportati in partitura per mezzo di un quadrato con all'interno inscritto il numero dello specifico multifonico, ad esempio per il primo \fbox{1}.\newline

\begin{enumerate}[label=\fbox{\arabic*}]

%uno
\item{
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{ \remove Time_signature_engraver      \hide Staff.BarLine
} {
\clef "treble_8"
<gis a' dis>2.^\markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one two three four five six))
           (lh . (b gis))
           (rh . ())
           )}
    
    
}

\end{lilypond}	
}

%due
\item{
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{ \remove Time_signature_engraver      \hide Staff.BarLine
} {
\clef "treble_8"

<cis, fis>2^\markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one two three ))
           (lh . (b ))
           (rh . (cis dis))
           )
    }
    
}

\end{lilypond}	
}

%tre
\item{
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{ \remove Time_signature_engraver      \hide Staff.BarLine
} {
\clef "treble_8"

<b, bes,>2^\markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (one  three four five ))
           (lh . (bes ))
           (rh . (ees))
           )
    }    
}

\end{lilypond}	
}

%quattro
\item{
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{ \remove Time_signature_engraver      \hide Staff.BarLine
} {

<e c a'>2^\markup {
    \override #'(size . 0.5)
       \woodwind-diagram
      
          #'flute
          #'((cc . (two five ))
           (lh . (b))
           (rh . (ees))
           )
    }
    
}

\end{lilypond}	
}


\end{enumerate}
}
\end{itemize}

\newpage
%===LIVE ELECTRONICS================
\subsection *{Live Electronics}
%================NOTAZIONE ELETTRONICA
\subsubsection *{Notazione dell'elettronica utilizzata in partitura}
\begin{itemize}
\item{Fixed Media\newline\newline
Notazione per far partire i vari fixed media, nell'esempio vi è riportata la registrazione numero 1.
\newline
\hspace*{0.99cm}
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{       \hide Staff.BarLine \hide Clef
} {
\time 7/4
\override Staff.StaffSymbol.line-count = #1 %a che serve? 
     \hide Clef
      \override TimeSignature.font-size = #6
       \numericTimeSignature

     s1^\markup{\huge \circle{1}}    
}

\end{lilypond}
%\includegraphics[width=20mm,height=20mm,keepaspectratio]{tr.png}
}

\item{
Live Multi Oscillator\newline\newline
Si fa uso di quattro di questi dispositivi e la presenza di uno di essi è segnalata per mezzo di un quadrato con all’interno inscritto il numero dello specifico oscillatore, ad esempio per il primo \fbox{1}, per ognuno di essi sono notati:
\begin{itemize}
\item{Frequenza dell'oscillatore, indicata all'inizio e variata nel tempo tramite linee continue per effettuare dei glissando\footnote{per maggiore precisione, osservare la frequenza di arrivo dell'oscillatore indicata superiormente alla fine della linea}}%linea continua
\item{Volume dell'oscillatore, indicato per mezzo della dinamica in basso}%dinamica in basso
\item{Frequenza del filtro passa alto del rumore bianco decorellato per i 4 canali, indicato sul secondo rigo all'inizio e variata nel tempo tramite linee tratteggiate per effettuare dei glissando\footnote{per maggiore precisione, osservare la frequenza di arrivo del filtro indicata superiormente alla fine della linea}}%linea tratteggiata
\item{Volume del rumore bianco, indicato per mezzo della dinamica in basso}%dinamica in basso
\end{itemize}
\vspace{0.5cm}
Ad esempio in caso vi siano 3 oscillatori con frequenza di partenza di 60 Hz e presenza di white noise degli stessi con filtro passa alto con frequenza iniziale 70 Hz:

\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

}

 \new Staff \with{       \hide Staff.BarLine \hide Clef
} {
\override Staff.StaffSymbol.line-count = #1
   \override Staff.StaffSymbol.line-positions = #'(0)
  %\override StaffSymbol.thickness = #(magstep 10)
   \hide Clef
     \numericTimeSignature
     %\override TextScript.extra-offset = #'(0 . 11.1)
     %\override Glissando.thickness = #'2
     \hide Clef
       \numericTimeSignature
       \time 7/4
        \override TimeSignature.font-size = #6
             %spazi lasciati per esteso e non raggruppati per poter inserire fixed media all'occorrenza in ogni posizione

   \hideNotes %<<{
      \override System.top-system-spacing = #'2
      %s1. s4
      %s1 s2.
      %s1 s2 s4 
    
      s1.\pp^\markup{\box \column{\line{Live Multi Oscillator   \box 1 \box 2 \box 3  60 Hz }
                                                         %\line {\box 2 70 Hz}
                                                         %\line{\box 3  90 Hz}
                                                         %\line {\box 4 100 Hz}
                                                          }}%_\markup{ \box 1  60 Hz}
           s4
       
    
   
}

\end{lilypond}
\vspace{0.5cm}
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

}

 \new Staff \with{       \hide Staff.BarLine \hide Clef
} {
\override Staff.StaffSymbol.line-count = #1
   \override Staff.StaffSymbol.line-positions = #'(0)
  %\override StaffSymbol.thickness = #(magstep 10)
   \hide Clef
     \numericTimeSignature
     %\override TextScript.extra-offset = #'(0 . 11.1)
     %\override Glissando.thickness = #'2
     \hide Clef
       \numericTimeSignature
       \time 7/4
        \override TimeSignature.font-size = #6
             %spazi lasciati per esteso e non raggruppati per poter inserire fixed media all'occorrenza in ogni posizione

   \hideNotes %<<{
      \override System.top-system-spacing = #'2
      %s1. s4
      %s1 s2.
      %s1 s2 s4 
    
      s1.\pp^\markup{\box \column{\line{White Noise Live Multi Oscillator: HPF 70 Hz}
                                                         %\line {\box 2 70 Hz}
                                                         %\line{\box 3  90 Hz}
                                                         %\line {\box 4 100 Hz}
                                                          }}%_\markup{ \box 1  60 Hz}
           s4
       
    
   
}

\end{lilypond}

%\fi%++++++++++++++++++++++++++++++++++++++
}
\item{Delay RM\newline\newline
Per esso si indica tramite la dinamica la presenza o l'assenza dello stesso, i millisecondi di ritardo da utilizzare sono definiti all'inizio dell'utilizzo della linea di ritardo sulla partitura, il controllo è definito globalmente per tutti i ritardi.
%\newline
%\hspace*{0.99cm}
%\includegraphics[width=20mm,height=20mm,keepaspectratio]{tr.png}
%\iffalse%++++++++++++++++++++++++++++++++++DISABILITATO
\begin{lilypond}[fragment,relative=2,quote,staffsize=16]
	
\version "2.22.2"

 \new Staff \with{       \hide Staff.BarLine \hide Clef
} {
\time 7/4
    \override Staff.StaffSymbol.line-count = #1
   \override Staff.StaffSymbol.line-positions = #'(0)
  %\override StaffSymbol.thickness = #(magstep 10)
  \hide Clef
     \numericTimeSignature
   \hideNotes
       \once \override Hairpin.circled-tip = ##t d1^\markup{\column \box{{\line{Delay 10 ms} }}}\< d2.
     d1 \once \override Hairpin.circled-tip = ##t  d2.\f\>
     s1\! s2.
}

\end{lilypond}
%\fi%++++++++++++++++++++++++++++++++++++++
}

\end{itemize}
\newpage
%===DIAGRAMMI ELETTRONICA==========
\subsubsection *{Diagrammi a blocchi}
Si fa uso all'interno del brano di alcuni strumenti elettronici di cui vengono riportati i diagrammi degli stessi per mezzo della descrizione realizzata da \textsc{Walter Branchi} nel testo \textit{Tecnologia della Musica Elettronica}\footnote{il testo non menziona l'utilizzo di ritardi che sono pertanto stati inseriti nei diagrammi come dei quadrati con la dicitura \textsc{Del} }.
\begin{itemize}
\item{Fixed Media\newline\newline
Registrazioni di alcuni frammenti suonati dal Flauto basso\footnote{meglio se registrati in Tetrarec o Ambisonics(per quest'ultimo fare riferimento all'apposita libreria per la riproduzione di un suono ambisonico all'interno di S.T.ONE)} riproposti durante il brano.
\newline
\hspace*{0.99cm}
\includegraphics[width=0.8\textwidth]{fixed_media_trasp_back.png}
}
\item{Live Multi Oscillator\newline\newline
Doppio Oscillatore realizzato con sintesi sottrattiva per mezzo di una coppia di filtri (filtro passa alto seguito da un filtro passa basso del 24mo ordine). La frequenza del filtro è calcolata in questo modo:\newline \verb|frequenza+((numero_dell'oscillatore+1)*(1.03)*(frequenza/(numero_dell'oscillatore+1)))|\newline  in cui la \verb|frequenza| viene impostata tramite apposito controllo dall'esecutore.   \newline
\hspace*{0.99cm}
\includegraphics[width=0.8\textwidth]{lmo_trasp_back.png}
}
\item{Delay RM\newline\newline
Linea di ritardo a due canali con primo canale con ritardo fisso e secondo canale con ritardo che realizza una Ring Modulation con il segnale del flauto in ingresso :
\newline
\hspace*{0.99cm}
\includegraphics[width=0.8\textwidth]{delrm_trasp_back.png}
}
\end{itemize}

\newpage
%============MATRICI DI INGRESSI ED USCITE=====

\subsection *{Matrici degli ingressi e delle uscite}
Riportiamo di seguito la matrici degli ingressi e delle uscite dei segnali, si fa uso nuovamente della schematizzazione suggerita da \textsc{Walter Branchi}\footnote{in questo caso si sono voluti astrarre gli strumenti elettronici  prima descritti (tramite un rettangolo che li contenesse) e si sono inseriti un divisore ed un sommatore di segnali (non presenti nella descrizione di \textsc{Branchi})}:
\begin{itemize}
\item{
Matrice del fixed media.
\newline
\includegraphics[width=0.7\textwidth]{matrice_ingressi_uscite-2.png}
}
\item{
Matrice del Live Multi Oscillator.
\newline
Versione con un unico Live Multi Oscillator (che ha al suo interno 8 oscillatori):
\newline
\includegraphics[width=0.7\textwidth]{matrice_ingressi_uscite-3.png}
\newpage
Versione con 4 Live Multi Oscillator (con ognuno con due oscillatori al suo interno), si sono omessi i collegamenti degli \textit{LMO 2}, \textit{LMO 3} ed \textit{LMO 4} che sono da farsi analogamente all'LMO 1, osservando che il primo canale di uscita di ciascun \textit{LMO} debba andare in un canale in entrata del sommatore del primo altoparlante e del sommatore del Subwoofer,  il secondo canale di uscita di ciascun \textit{LMO} debba andare in un canale in entrata del sommatore del secondo altoparlante e del sommatore del Subwoofer, etc...
\newline
\includegraphics[width=0.7\textwidth]{matrice_ingressi_uscite-4.png}
}
\item{
Matrice del Delay con Ring Modulation.
\newline
\includegraphics[width=0.7\textwidth]{matrice_ingressi_uscite-1.png}
}
\end{itemize}


\newpage

%===================================
%==============PARTITURA===========
%===================================

%Include della partitura completa

%macOS

%Include della partitura completa
\includepdf [pages=-]{/Users/davide/gitlab/DavideTedesco/estensione_numero_1/docs/Davide_Tedesco-Studio_sul_Corpo_dOmbra_Numero_1-lilypond/Davide_Tedesco-Studio_sul_Corpo_dOmbra_Numero_1_flauto_suoni_trasposti.pdf}


\end{document}