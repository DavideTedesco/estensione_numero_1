%% http://lsr.dsi.unimi.it/LSR/Item?id=1050
%% see also: http://lsr.dsi.unimi.it/LSR/Item?id=1026
%% discussion: http://lilypond.1069038.n5.nabble.com/need-markup-function-for-arrow-at-end-of-curve-td200752.html

%here starts the snippet:

#(define-markup-command 
   (curvedArrow layout props 
     arrow-length ;; any number >=0
     height ;; any number >=0
     strait-length ;; any number >=0
     )(number? number? number?)
   "Draw a curved arrow with variable length and height."
     (interpret-markup layout props
       (markup #:with-dimensions (cons 0 0) (cons 0 0)
         (#:override (cons 'filled #t)
           (#:path 0.13
             `((curveto 0 ,(+ .05 (abs height)) ,(+ 1.4 (abs arrow-length)) ,(+ 1.65 (abs height)) ,(+ 5.62 (abs arrow-length)) ,(+ 1.65 (abs height)))
               (rlineto ,(abs strait-length) 0)
               (rcurveto 0 .2 -.1 .2 -.1 .45)
               (rcurveto 0 .05 .6 -.25 .9 -.35)
               (rcurveto .3 -.1 .75 -.2 .75 -.3)
               (rcurveto 0 -.1 -.45 -.15 -.75 -.25)
               (rcurveto -.35 -.1 -.9 -.4 -.9 -.35)
               (rcurveto 0 .25 .1 .25 .1 .45)
               (rlineto ,(- (abs strait-length)) 0)
               (curveto ,(+ 1.55 (abs arrow-length)) ,(+ 1.3 (abs height)) 0 ,(+ -.05 (abs height)) 0 0)
               (closepath)))))))

%% Test:
{
 c'^\markup\curvedArrow #0 #0 #0
 c'
 c'_\markup\scale #'(-1 . -1)\rotate #-10 \curvedArrow #3 #3 #5
}

