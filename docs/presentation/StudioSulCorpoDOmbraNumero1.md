---
markmap:
  initialExpandLevel: 0
---

<!--> https://davidetedesco.gitlab.io/estensione_numero_1/mindmap.html?md=StudioSulCorpoDOmbraNumero1.md <-->

# _Studio sul Corpo d'Ombra #1_

## Introduzione
### Il progetto compositivo complessivo
### Le origini
#### Arthur Rimbaud - _Voyelles_
#### Fausto Romitelli - _Golfi d'ombra_
### Il testo
#### _Corpi d'ombra_
### Perchè una raccolta di studi?

## Organico
### Flauto Basso
### Live Electronics
#### Live Multi Oscillator
#### Fixed Media
#### Delay con RM

## Il percorso compositivo
### La scrittura strumentale e l'elettronica
### Il posizionamento nello spazio
### L'amplificazione

## La Sostenibilità della Musica Elettro-Acustica
### Partitura
#### Utilizzo di strumenti grafici e di scrittura FLOSS
##### Lilypond
##### $\LaTeX$
##### Xournal++
### Live Electronics
#### Utilizzo di strumenti per l'elettronica FLOSS
##### FAUST
##### Reaper

## Prossimi passi
### _Studio sul Corpo d'Ombra #2_
#### Per Flauto Basso e Voce Maschile (senza elettronica)
#### Legame tra voce maschile e flauto basso
### _Studio sull'Ombra del Corpo #1_
#### Per voce maschile e Live Electronics
#### Legame tra voce maschile e Live Electronics
### Studio della messa in scena
#### Studio drammaturgico
#### Studio illuminotecnico
