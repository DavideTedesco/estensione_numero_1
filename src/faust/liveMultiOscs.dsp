declare name "liveMultiOscs - 4ch decorrelated live subtractive oscillator controllable";
declare vendor "Davide Tedesco";
import("stdfaust.lib");

masterVolume = (hslider("Master Volume",0.997,0,0.998,0.001)) : si.smoo;
volumeLMO = (hslider("LMO Volume",0,0,0.998,0.001)) : si.smoo;
directnoisevolume = (hslider("Direct Noise Volume",0,0,0.3,0.001)) : si.smoo;
freqSlider = (hslider("Frequency LMO",40,20,3000,0.001)) : si.smoo;
freqHPNoise = (hslider("Frequency High Pass Noise",20,20,5000,0.001)) : si.smoo;
numberOfOscillators = 2;
filtersOrder = 24;

//vumeters
vumeter = hmeter(1), hmeter(2), hmeter(3), hmeter(4)
with {
	vmeter(i, x) = attach(x, envelop(x) : vbargraph("chan %i[2][unit:dB]", -70, +5));
	hmeter(i, x) = attach(x, envelop(x) : hbargraph("chan %i[2][unit:dB]", -70, +5));
	envelop = abs : max ~ -(1.0/ma.SR) : max(ba.db2linear(-70)) : ba.linear2db;
};

//TEST no.multinoise(4)
//process = no.multinoise(4) : par(i,4, _*directnoisevolume);

//4ch decorrelated live multi oscillator
multioscil(frequency,filterOrder) = no.multinoise(8) : (par(i,4, _ :  fi.highpass(filterOrder,frequency+i) : fi.lowpass(filterOrder,frequency+i-0.0001)  )),_,_,_,_ : _,_,_,_,par(i,4, _:fi.highpass(filterOrder,freqHPNoise)) ;

//TEST SINGLE MULTIOSCIL
//process = multioscil(1000,24) : par(i,4, _*volume), par(i,4, _*directnoisevolume) :> _,_,_,_;

//multioscils(numberOfOscillators, filtersOrder) = multioscil1;//,multioscil2 :> _,_,_,_ ;
multioscils(freq,n,fo) = par(i,n,multioscil(freq+((i+1)*(1.03)*(freq/(i+1))),fo) ) :>  par(i,8, fi.dcblockerat(20) :   fi.integrator : co.compressor_mono(8,-12,1,1)) : par(i,4, _*volumeLMO),par(i,4, _*directnoisevolume) :> _,_,_,_ ;
//alternative way to switch ON/OFF individual channels
//: _*(checkbox("1 - LFU"))*volume,_*(checkbox("2 - RFD"))*volume,_*(checkbox("3 - RBU"))*volume,_*(checkbox("4 - LBD"))*volume;

//resulting frequency 2*chosenFrequency
process = multioscils(freqSlider/2,numberOfOscillators,filtersOrder) : par(i,4, _*masterVolume) : vumeter;
