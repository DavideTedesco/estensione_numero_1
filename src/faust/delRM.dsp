declare name "delRM - 4ch delay line with RM on 2nd and 4th channel";
declare vendor "Davide Tedesco";
import("stdfaust.lib");

//delay max
nmax = 384000;

n = hslider("Delay",0.022,0.001,5,0.001) ;
masterVolume = (hslider("Master Volume",0.90,0,0.998,0.001)) : si.smoo;


//inmeters
inmeter = hmeter(1), hmeter(2)
with {
	vmeter(i, x) = attach(x, envelop(x) : vbargraph("in %i[2][unit:dB]", -70, +5));
	hmeter(i, x) = attach(x, envelop(x) : hbargraph("in %i[2][unit:dB]", -70, +5));
	envelop = abs : max ~ -(1.0/ma.SR) : max(ba.db2linear(-70)) : ba.linear2db;
};

//vumeters
vumeter = hmeter(1), hmeter(2), hmeter(3), hmeter(4)
with {
	vmeter(i, x) = attach(x, envelop(x) : vbargraph("chan %i[2][unit:dB]", -70, +5));
	hmeter(i, x) = attach(x, envelop(x) : hbargraph("chan %i[2][unit:dB]", -70, +5));
	envelop = abs : max ~ -(1.0/ma.SR) : max(ba.db2linear(-70)) : ba.linear2db;
};
//DELAY WITH RM ON SECOND AND FOURTH CHANNEL
delRM = ( _ <: de.delay(ba.sec2samp(n),nmax),_ :> _),( _ <: de.delay(ba.sec2samp(n),nmax)*_*fi.integrator   :> _) : _,(_*10 : co.compressor_mono(11,-24,0.03,0.04))<: _,_,_,_ : par(i, 4, _ : fi.dcblocker);

process = _,_ : inmeter : delRM : par(i,4, _*masterVolume) : vumeter; 
